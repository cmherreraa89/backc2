<?php
$loc = "maqu";
include_once("src/mobiles.php");
require_once("src/seguridad.php");
?>
<!DOCTYPE html>
<html>
<head>
  <?php include_once("src/header.php"); ?>
  <style type="text/css" media="screen">
    @media (min-width: 992px) {
        .modal-lg {
            width: 1200px;
            height: 600px; /* control height here */
        }
    }

    .modal img {
          max-width: 25%;
          border-radius: 75px;
          /*border: 1px solid #aaa;*/
      }

      #imagen > p{
        color:#4ebfbb;
        cursor: pointer;
        font-weight: bold;
      }
  </style>
</head>
<body class="hold-transition skin-green sidebar-mini">
<div class="wrapper">
  <?php include_once("src/main_header.php"); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Maquinaria
        <small>Se puede observar en este módulo las operaciones para la administración de la maquinaria disponible al servicio de Grupo C2 bien sea propia o alquilada destinada o no a algún frente de obra.</small>
      </h1>
      <!--<ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
      </ol>-->
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
    <!-- Main content -->

    <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"><button type="button" id="crear" class="btn btn-info btn-xs" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus-square"></i> Agregar Máquina</button></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="table-trabajadores" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Fotografía</th>
                  <th>Tipo</th>
                  <th>Placa(s)</th>
                  <th>Tránsito</th>
                  <th>Frente de Obra</th>
                  <th>Propiedad</th>
                  <th>Observaciones</th>
                  <th>Localización</th>
                  <th>Operador</th>
                  <th>Estado</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
                <!-- <tr>
                  <td style="text-align: center;">
                        <img src="../dist/img/maquinaria/retroexcavadora.png" style="height: 80px;" alt="Maquinaria">
                  </td>
                  <td>Retroexcavadora</td>
                  <td></td>
                  <td><span class="small">SOAT: 2019-10-12<br>TECN: 2019-12-10</span></td>
                  <td><span class="small">Remodelación Parque la Ceiba</span></td>
                  <td><span class="text-warning">Alquilado(a)</span></td>
                  <td><span class="text-danger small">Próximo cambio de aceite: 15.400 KM</span></td>
                  <td>En Obra</td>
                  <td><i class="fa fa-user"></i> Diego A Lamprea M</td>
                  <td><span class="text-success">Activo</span></td>
                  <td>
                    <button type="button" class="btn btn-success btn-xs"><i class="fa fa-edit"></i></button>
                    <button type="button" class="btn btn-warning btn-xs"><i class="fa fa-calendar"></i></button>
                    <button type="button" class="btn btn-info btn-xs"><i class="fa fa-wrench"></i></button>
                    <button type="button" class="btn btn-danger btn-xs"><i class="fa fa-image"></i></button>
                  </td>
                </tr> -->
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Modal -->
      <div class="modal fade" id="myModal" role="dialog">
                      <div class="modal-dialog modal-lg">

                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" id="tipo-modal">Datos actuales de la maquina</h4>
                          </div>
                          <!-- Inicio formulario -->
                                <form autocomplete="off" id="frm">
                                      <div class="modal-body">

                                              <div class="row">
                                                <div class="col-md-3"></div>
                                                <div class="col-md-6">
                                                      <center>
                                                          <div id="imagen">
                                                            <img id="photo" src="../dist/img/maquinaria/maquina-default.png" />
                                                            <input accept="image/*" name="file-photo" id="file-photo" type="file" style="display: none;" />
                                                            <p id="editar-imagen">Editar <i class="fa fa-edit"></i></p>
                                                          </div>
                                                        </center>
                                                </div>
                                                <div class="col-md-3"></div>
                                              </div>

                                              <div class="row">

                                                <div class="col-md-3">
                                                  <!-- select -->
                                                    <div class="form-group">
                                                      <label for="tipomaquina_id">Tipo</label>
                                                      <select id="tipomaquina_id" name="tipomaquina_id" data-placeholder="Seleccionar" class="select2 form-control" tabindex="-1" required>
                                                      </select>
                                                    </div>
                                                    <!-- select -->
                                                </div>

                                                <div class="col-md-3">
                                                  <div class="form-group">
                                                    <label for="placa">PLaca</label>
                                                    <input type="text" class="form-control" id="placa" name="placa" placeholder="" autocomplete="off" required>
                                                  </div>
                                                </div>

                                                <div class="col-md-3">
                                                  <div class="form-group">
                                                    <label for="ult_soat">Ult. SOAT</label>
                                                    <input type="text" class="form-control" id="ult_soat" name="ult_soat" placeholder="" autocomplete="off" required>
                                                  </div>
                                                </div>

                                                <div class="col-md-3">
                                                  <div class="form-group">
                                                    <label for="ult_tecno">Ult. Tecno</label>
                                                    <input type="text" class="form-control" id="ult_tecno" name="ult_tecno" placeholder="" autocomplete="off" required>
                                                  </div>
                                                </div>

                                              </div>

                                              <div class="row">

                                                <div class="col-md-3">
                                                  <div class="form-group">
                                                    <label for="prox_man">Prox. Mantenimiento</label>
                                                    <input type="text" class="form-control" id="prox_man" name="prox_man" placeholder="" autocomplete="off" required>
                                                  </div>
                                                </div>

                                                <div class="col-md-3">
                                                   <!-- select -->
                                                    <div class="form-group">
                                                      <label for="frenteobra_id">Frente obra</label>
                                                      <select id="frenteobra_id" name="frenteobra_id" data-placeholder="Seleccionar" class="select2 form-control" tabindex="-1" required>
                                                      </select>
                                                    </div>
                                                    <!-- select -->
                                                </div>

                                                <div class="col-md-3">
                                                    <!-- select -->
                                                      <div class="form-group">
                                                        <label for="propiedad">Propiedad</label>
                                                        <select id="propiedad" name="propiedad" data-placeholder="Selecionar" class="select2 form-control" tabindex="-1" required>
                                                          <option value=""></option>
                                                          <option value="Propio">Propio</option>
                                                          <option value="Alquilado">Alquilado</option>
                                                        </select>
                                                      </div>
                                                      <!-- select -->
                                                </div>

                                                <div class="col-md-3">
                                                  <!-- select -->
                                                      <div class="form-group">
                                                        <label for="localizacion">Localización</label>
                                                        <select id="localizacion" name="localizacion" data-placeholder="Selecionar" class="select2 form-control" tabindex="-1" required>
                                                          <option value=""></option>
                                                          <option value="En obra">En obra</option>
                                                          <option value="En circulacion">En circulacion</option>
                                                          <option value="En reparacion">En reparacion</option>
                                                          <option value="En parqueadero">En parqueadero</option>
                                                        </select>
                                                      </div>
                                                      <!-- select -->
                                                </div>
                                              </div>

                                              <!-- row -->
                                               <div class="row">

                                                <div class="col-md-9">
                                                  <div class="form-group">
                                                        <label for="estado_id">Observaciones</label>
                                                        <textarea rows="2" cols="50" class="form-control" id="observaciones" name="observaciones">
                                                        </textarea>
                                                      </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <!-- select -->
                                                      <div class="form-group">
                                                        <label for="estado_id">Estado</label>
                                                        <select id="estado_id" name="estado_id" data-placeholder="Seleccionar" class="select2 form-control" tabindex="-1" required>
                                                        </select>
                                                      </div>
                                                      <!-- select -->
                                                </div>
                                              </div>
                                              <!-- ./row -->
                                      </div>
                                      <div class="modal-footer">
                                        <button id="enviar-datos" type="submit" class="btn btn-info">Actualizar</button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                      </div>

                                </form>
                                <!-- Fin formulario -->
                          <!-- End Modal content-->
                        </div>
                      </div>
                    </div>
        <!--/ Modal -->


        <!-- Modal_archivos -->
                      <div class="modal fade" id="Modal_archivos" role="dialog">
                        <div class="modal-dialog">

                          <!-- Modal content-->
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                              <h4 class="modal-title" id="tipo-modal">Archivos del usuario</h4>
                            </div>
                            <div class="modal-body">

                              <!-- Inicio formulario -->
                                  <form autocomplete="off" id="frm_archivo">

                                    <div class="row">
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="nombre">Nombre archivo</label>
                                          <input type="text" class="form-control" id="nombre" name="nombre" autocomplete="off" required>
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                          <div class="form-group">
                                            <label for="archivo">Seleccionar archivo</label>
                                            <input type="file" class="form-control" id="archivo" name="archivo" accept="application/pdf" autocomplete="off" required>
                                          </div>
                                      </div>
                                    </div>

                                    <button type="submit" class="btn btn-info">Agregar</button>
                                  </form>
                                  <!-- Fin formulario -->

                                  <!-- Inicio table -->
                                    <h2>Archivos de este usuario</h2>
                                    <!-- <p>The .table class adds basic styling (light padding and only horizontal dividers) to a table:</p> -->
                                    <div class="table-responsive">
                                    <table id="table_archivos" class="table">
                                      <thead>
                                        <tr>
                                          <th>Nombre</th>
                                          <th>Acciones</th>
                                        </tr>
                                      </thead>
                                      <tbody>

                                      </tbody>
                                    </table>
                                  </div>
                              <!-- fin tabla -->

                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            </div>

                            <!-- End Modal content-->
                          </div>
                        </div>

                       </div>
                       <!--Fin  Modal archivos -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include_once("src/main_footer.php"); ?>
</div>
<!-- ./wrapper -->
<?php include_once("src/footer.php"); ?>
<script src="js/maquinaria.js?v=<?php echo $sin_cache ?>"></script>
</body>
</html>