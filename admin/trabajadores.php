<?php
$loc = "trab";
include_once("src/mobiles.php");
require_once("src/seguridad.php");
?>
<!DOCTYPE html>
<html>
<head>
  <?php include_once("src/header.php"); ?>
  <style type="text/css" media="screen">
    @media (min-width: 992px) {
        .modal-lg {
            width: 1200px;
            height: 600px; /* control height here */
        }
    }

    .modal img {
          max-width: 25%;
          border-radius: 75px;
          /*border: 1px solid #aaa;*/
      }

      #imagen > p{
        color:#4ebfbb;
        cursor: pointer;
        font-weight: bold;
      }
  </style>

</head>
<body class="hold-transition skin-green sidebar-mini">
<div class="wrapper">
  <?php include_once("src/main_header.php"); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Personal
        <small>Sea o no personal de la empresa, aquí se listan los trabajadores que notifican horario de trabajo, sin embargo este módulo permite la administración de datos de los empleados mas no el control de novedades</small>
      </h1>
      <!--<ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
      </ol>-->
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
    <!-- Main content -->

    <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"><button id="crear" type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus-square"></i> Agregar Trabajador</button></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="table-trabajadores" class="table table-bordered table-striped display" style="width:100%">
                    <thead>
                        <tr>
                            <th>Foto</th>
                            <th>Nombres</th>
                            <th>Apellidos</th>
                            <th>Cédula</th>
                            <th>Empresa</th>
                            <th>Cargo</th>
                            <th>Fecha ingreso</th>
                            <th>Bienestar</th>
                            <th>Contacto</th>
                            <th>Estado</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Modal -->
      <div class="modal fade" id="myModal" role="dialog">
                      <div class="modal-dialog modal-lg">

                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" id="tipo-modal">Datos actuales del usuario</h4>
                          </div>
                          <!-- Inicio formulario -->
                                <form autocomplete="off" id="frm">
                                      <div class="modal-body">

                                              <div class="row">
                                                <div class="col-md-3"></div>
                                                <div class="col-md-6">
                                                      <center>
                                                          <div id="imagen">
                                                            <img id="photo" src="../dist/img/usuarios/default-profile.png" />
                                                            <input accept="image/*" name="file-photo" id="file-photo" type="file" style="display: none;" />
                                                            <p id="editar-imagen">Editar <i class="fa fa-edit"></i></p>
                                                          </div>
                                                        </center>
                                                </div>
                                                <div class="col-md-3"></div>
                                              </div>

                                             <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                      <label for="nombres">Nombres</label>
                                                      <input type="text" class="form-control" id="nombres" name="nombres" placeholder="" autocomplete="off" required>
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                  <div class="form-group">
                                                    <label for="apellidos">Apellidos</label>
                                                    <input type="text" class="form-control" id="apellidos" name="apellidos" placeholder="" autocomplete="off" required>
                                                  </div>
                                                </div>

                                                <div class="col-md-3">
                                                  <!-- select -->
                                                    <div class="form-group">
                                                      <label for="ciudad_nacimiento">Lugar de nacimiento</label>
                                                      <select id="ciudad_nacimiento" name="ciudad_nacimiento" data-placeholder="Seleccionar Ciudad" class="select2 form-control" tabindex="-1" required>
                                                      </select>
                                                    </div>
                                                    <!-- select -->
                                                </div>



                                             
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                      <label for="fecha_nacimiento">Fecha de nacimiento</label>
                                                      <input type="text" class="form-control" id="fecha_nacimiento" name="fecha_nacimiento" placeholder="" autocomplete="off"  required>
                                                    </div>
                                                </div>

                                              </div>

                                      <!-- campos agregados -->
                                              <div class="row">

                                                <div class="col-md-3">
                                                  <!-- select -->
                                                    <div class="form-group">
                                                      <label for="tipo_identificacion_id">Tipo identificación</label>
                                                      <select id="tipo_identificacion_id" name="tipo_identificacion_id" data-placeholder="Seleccionar Ti" class="select2 form-control" tabindex="-1" required>
                                                      </select>
                                                    </div>
                                                    <!-- select -->
                                                </div>


                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                      <label for="identificacion">Nº Identificaci&oacute;n</label>
                                                      <input type="number" class="form-control" id="identificacion" name="identificacion" placeholder="" autocomplete="off" readonly required>
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                  <!-- select -->
                                                    <div class="form-group">
                                                      <label for="ciudad_exp">Lugar de expedición </label>
                                                      <select id="ciudad_exp" name="ciudad_exp" data-placeholder="Seleccionar Ciudad" class="select2 form-control" tabindex="-1" required>
                                                      </select>
                                                    </div>
                                                    <!-- select -->
                                                </div>
                                             
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                      <label for="fecha_expedicion">Fecha de expedición</label>
                                                      <input type="text" class="form-control" id="fecha_expedicion" name="fecha_expedicion" placeholder="" autocomplete="off"  required>
                                                    </div>
                                                </div>

                                              </div>
                                              <!--campos agregados FIN -->

                                              <div class="row">
                                                <div class="col-md-3">
                                                   <!-- select -->
                                                    <div class="form-group">
                                                      <label for="empresa_id">Empresa</label>
                                                      <select id="empresa_id" name="empresa_id" data-placeholder="Seleccionar empresa" class="select2 form-control" tabindex="-1" required>
                                                      </select>
                                                    </div>
                                                    <!-- select -->
                                                </div>

                                                <div class="col-md-3">
                                                  <div class="form-group">
                                                    <label for="cargo">Cargo</label>
                                                    <input type="text" class="form-control" id="cargo" name="cargo" placeholder="" autocomplete="off" required>
                                                  </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                      <label for="fecha_ingreso">Fecha de ingreso</label>
                                                      <input id="fecha_ingreso" name="fecha_ingreso" type="text" class="data-picker-single form-control" placeholder="" />
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                  <!-- select -->

                                                  <div class="form-group">
                                                      <label for="ciudad_id">Ciudad</label>
                                                      <select id="ciudad_id" name="ciudad_id" data-placeholder="Selecionar ciudad" class="select2 form-control" tabindex="-1" required>
                                                      </select>
                                                    </div>
                                                      
                                                      <!-- select -->
                                                </div>
                                              </div>

                                              <div class="row">
                                                <div class="col-md-3">
                                                   <!-- select -->
                                                    <div class="form-group">
                                                      <label for="eps_id">Eps</label>
                                                      <select id="eps_id" name="eps_id" data-placeholder="Selecionar eps" class="select2 form-control" tabindex="-1" required>
                                                      </select>
                                                    </div>
                                                    <!-- select -->
                                                </div>

                                                <div class="col-md-3">
                                                    <!-- select -->
                                                    <div class="form-group">
                                                        <label for="arl_id">Arl</label>
                                                        <select id="arl_id" name="arl_id" data-placeholder="Selecionar arl" class="select2 form-control" tabindex="-1" required>
                                                        </select>
                                                   </div>
                                                    <!-- select -->
                                                </div>

                                                <div class="col-md-3">
                                                    <!-- select -->
                                                      <div class="form-group">
                                                        <label for="afp_id">Afp</label>
                                                        <select id="afp_id" name="afp_id" data-placeholder="Seleccionar afp" class="select2 form-control" tabindex="-1" required>
                                                        </select>
                                                      </div>
                                                    <!-- select -->
                                                </div>
                                                

                                                 <div class="col-md-3">
                                                    <!-- select -->
                                                    <div class="form-group">
                                                      <label for="ccf_id">Caja de compensación</label>
                                                      <select id="ccf_id" name="ccf_id" data-placeholder="Selecionar caja de compensación" class="select2 form-control" tabindex="-1" required>
                                                      </select>
                                                    </div>
                                                    
                                                      <!-- select -->
                                                </div>
                                              </div>

                                              <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                      <label for="telefono">Télefono</label>
                                                      <input type="number" class="form-control" id="telefono" name="telefono" placeholder=""  autocomplete="off" required>
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                      <label for="celular">Celular</label>
                                                      <input type="number" class="form-control" id="celular" name="celular" placeholder=""  autocomplete="off" required>
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                      <label for="email">Email</label>
                                                      <input type="email" class="form-control" id="email" name="email" placeholder=""  autocomplete="off" required>
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <!-- select -->
                                                      <div class="form-group">
                                                        <label for="estado_id">Estado</label>
                                                        <select id="estado_id" name="estado_id" data-placeholder="Selecionar Estado" class="select2 form-control" tabindex="-1" required>
                                                        </select>
                                                      </div>
                                                      <!-- select -->
                                                </div>
                                              </div>

                                              <!-- row -->
                                               <div class="row">

                                                  <div class="col-md-3">
                                                    <div class="form-group">
                                                      <label for="direccion">Dirección</label>
                                                      <input type="text" class="form-control" id="direccion" name="direccion" placeholder="" autocomplete="off" required>
                                                    </div>
                                                </div>
                                               <div class="col-md-3">
                                                   
                                                     <div class="form-group">
                                                      <label for="barrio">Barrio</label>
                                                      <input type="text" class="form-control" id="barrio" name="barrio" placeholder="" autocomplete="off" required>
                                                    </div>
                                                   
                                                </div>

                                                <div class="col-md-3">
                                                  <div class="form-group">
                                                      <label for="nomina">Nómina</label>
                                                      <input type="text" class="form-control" id="nomina" name="nomina" placeholder="" autocomplete="off" required>
                                                    </div>
                                                    <!-- select -->
                                                      <!-- <div class="form-group">
                                                        <label for="nomina">Nomina</label>
                                                        <select id="nomina" name="nomina" data-placeholder="Seleccionar afp" class="select2 form-control" tabindex="-1" required>
                                                        </select>
                                                      </div> -->
                                                    <!-- select -->
                                                </div>

                                                <div class="col-md-3">
                                                    <!-- select -->
                                                      <div class="form-group">
                                                        <label for="perfil_id">Perfil</label>
                                                        <select id="perfil_id" name="perfil_id" data-placeholder="Seleccionar perfil" class="select2 form-control" tabindex="-1" required>
                                                          <option value=""></option>
                                                          <option value="1">Administrador</option>
                                                          <option value="2">Residente</option>
                                                          <option value="3">Conductor</option>
                                                          <option value="4">Mecánico</option>
                                                          <option value="5">Consultor</option>
                                                        </select>
                                                      </div>
                                                      <!-- select -->
                                                </div>
                                              </div>
                                              <!-- ./row -->

                                              <!-- row -->
                                               <div class="row">

                                                <div class="col-md-3">
                                                    
                                                     <div class="form-group">
                                                      <label for="numero_cuenta">Nº Cuenta bancaria</label>
                                                      <input type="number" class="form-control" id="numero_cuenta" name="numero_cuenta" placeholder="" autocomplete="off" readonly required>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <!-- select -->
                                                      <div class="form-group">
                                                        <label for="tipo_cuenta">Tipo de cuenta bancaria</label>
                                                        <input type="text" class="form-control" id="tipo_cuenta" name="tipo_cuenta" placeholder="" autocomplete="off" required>
                                                      </div>
                                                    <!-- select -->
                                                </div>

                                                   <div class="col-md-3">
                                                 

                                                    <!-- select -->
                                                      <div class="form-group">
                                                        <label for="banco">Banco</label>
                                                        <select id="banco" name="banco" data-placeholder="Seleccionar banco" class="select2 form-control" tabindex="-1" required>
                                                        </select>
                                                      </div>
                                                    <!-- select -->
                                                   
                                                </div>

                                                

                                                <div class="col-md-3">
                                                
                                                      <div class="form-group">
                                                      <label for="sueldo_real">Sueldo real</label>
                                                      <input type="number" class="form-control" id="sueldo_real" name="sueldo_real" placeholder="" autocomplete="off" required>
                                                    </div>
                                                    
                                                </div>
                                              </div>
                                              <!-- ./row -->

                                               <!-- row -->
                                             <div class="row">

                                               <div class="col-md-3">
                                                 
                                                     <div class="form-group">
                                                      <label for="bonificacion">Bonificación</label>
                                                      <input type="number" class="form-control" id="bonificacion" name="bonificacion" placeholder="" autocomplete="off" required>
                                                    </div>
                                                   
                                                 </div>


                                                <div class="col-md-3">
                                                    <!-- select -->
                                                      <div class="form-group">
                                                        <label for="tipo_bonificacion"> Tipo de bonificación</label>
                                                        <select id="tipo_bonificacion" name="tipo_bonificacion" data-placeholder="Seleccionar afp" class="select2 form-control" tabindex="-1" required>
                                                          <option value="0">Sin bono</option>
                                                          <option value="1">Una sola vez al mes</option>
                                                          <option value="2">Por hora</option>                                                        </select>
                                                      </div>
                                                    <!-- select -->
                                                </div>
                                                

                                                   <div class="col-md-3">
                                                 
                                                     <div class="form-group">
                                                      <label for="tarifa_arl">Tarifa arl</label>
                                                      <input type="number" class="form-control" id="tarifa_arl" name="tarifa_arl" placeholder="" autocomplete="off" required>
                                                    </div>
                                                   
                                                </div>

                                                

                                                <div class="col-md-3">
                                                
                                                      <div class="form-group">
                                                      <label for="fecha_retiro">Fecha de retiro</label>
                                                      <input type="text" class="form-control" id="fecha_retiro" name="fecha_retiro" placeholder="" autocomplete="off" required>
                                                    </div>
                                                    
                                                </div>
                                              </div>
                                              <!-- ./row -->

                                               <!-- row -->
                                               <div class="row">
                                                  <div class="col-md-3"> 
                                                      <!-- select -->
                                                      <div class="form-group">
                                                        <label for="genero">Genero</label>
                                                        <select id="genero" name="genero" data-placeholder="Seleccionar perfil" class="select2 form-control" tabindex="-1" required>
                                                          <option value=""></option>
                                                          <option value="M">Masculino</option>
                                                          <option value="F">Femenino</option>
                                                          
                                                        </select>
                                                      </div>
                                                      <!-- select -->
                                                  </div>
                                                  <div class="col-md-3"> </div>
                                                  <div class="col-md-3"> </div>
                                                  <div class="col-md-3"> </div>
                                               </div>
                                                <!-- ./row -->
                                      </div>
                                      <div class="modal-footer">
                                        <button id="enviar-datos" type="submit" class="btn btn-info">Actualizar</button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                      </div>

                                </form>
                                <!-- Fin formulario -->
                          <!-- End Modal content-->
                        </div>
                      </div>
                    </div>
        <!--/ Modal -->


        <!-- Modal_archivos -->
                      <div class="modal fade" id="Modal_archivos" role="dialog">
                        <div class="modal-dialog">

                          <!-- Modal content-->
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                              <h4 class="modal-title" id="tipo-modal">Archivos del usuario</h4>
                            </div>
                            <div class="modal-body">

                              <!-- Inicio formulario -->
                                  <form autocomplete="off" id="frm_archivo">

                                    <div class="row">
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="nombre">Nombre archivo</label>
                                          <input type="text" class="form-control" id="nombre" name="nombre" autocomplete="off" required>
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                          <div class="form-group">
                                            <label for="archivo">Seleccionar archivo</label>
                                            <input type="file" class="form-control" id="archivo" name="archivo" accept="application/pdf" autocomplete="off" required>
                                          </div>
                                      </div>
                                    </div>

                                    <button type="submit" class="btn btn-info">Agregar</button>
                                  </form>
                                  <!-- Fin formulario -->

                                  <!-- Inicio table -->
                                    <h2>Archivos de este usuario</h2>
                                    <!-- <p>The .table class adds basic styling (light padding and only horizontal dividers) to a table:</p> -->
                                    <div class="table-responsive">
                                    <table id="table_archivos" class="table">
                                      <thead>
                                        <tr>
                                          <th>Nombre</th>
                                          <th>Acciones</th>
                                        </tr>
                                      </thead>
                                      <tbody>

                                      </tbody>
                                    </table>
                                  </div>
                              <!-- fin tabla -->

                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            </div>

                            <!-- End Modal content-->
                          </div>
                        </div>

                       </div>
                       <!--Fin  Modal archivos -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include_once("src/main_footer.php"); ?>
</div>
<!-- ./wrapper -->
<?php include_once("src/footer.php"); ?>
<script src="js/trabajador.js?v=<?php echo $sin_cache ?>"></script>
</body>
</html>