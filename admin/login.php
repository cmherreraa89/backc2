<?php
include_once("src/mobiles.php");
@session_start();
if(isset($_GET['UserId']) && isset($_GET['opcn'])){
  if($_GET['UserId']>0 && $_GET['opcn']=="LoguearUsuario"){
      setcookie('cookieGrupoC2', MD5($_GET['UserId'].'GrupoC22019'), time() + 365 * 24 * 60 * 60,'/','grupoc2sas.com',true,true);
      setcookie('UrlActGrupoC2', 'home', time() + 365 * 24 * 60 * 60,'/','grupoc2sas.com',true,true);
      header("location: home");
  }
}
if(isset($_SESSION['user'])){
  header("location: home");
}
include_once('controllers/login.php');

?>
<!DOCTYPE html>
<html lang="es" class="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Grupo C2 | PMOC (Parámetros de Mantenimiento de Operaciones Civiles)</title>
    <link rel="icon" type="image/x-icon" href="../dist/img/GrupoC2.ico">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="../plugins/iCheck/square/blue.css">
    <link rel="stylesheet" href="../dist/css/skins/skin-green.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="hold-transition skin-green login-page" style="background-image: url('../dist/img/background.jpg');background-repeat:no-repeat;background-size: cover;">
    <div class="row">
        <div class="col-md-9"></div>
        <div class="col-md-3" style="background-color: #ecf0f5; height: 985px;padding-top: 250px;opacity: 0.93;">
            <div class="login-box">
                <div class="login-logo">
                    <img src="../dist/img/GrupoC2.png" style="width: 300px" alt="Grupo C2">
                </div>
                <!-- /.login-logo -->
                <div class="login-box-body">
                    <p class="login-box-msg">Ingresa los datos para iniciar sesión</p>

                    <form id="FormLogin" action="login" method="post">
                        <div class="form-group has-feedback">
                            <input type="number" id="identificacion" name="identificacion" class="form-control" placeholder="Cédula" required>
                            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="password" id="password" name="password" class="form-control" placeholder="Contraseña" required>
                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        </div>
                        <div class="row">
                            <div class="col-xs-8">
                                <div class="checkbox icheck">
                                    <!--<label>
                                        <input type="checkbox"> Recordarme
                                    </label>-->
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-xs-4">
                                <input type="hidden" name="opcn" value="<?php echo(md5("GrupoC22019".date("Y-M-D")));?>">
                                <button type="submit" class="btn btn-success btn-block btn-flat">Ingresar</button>
                            </div>
                            <!-- /.col -->
                        </div>
                        <?php if( isset($usuario) && $usuario==0 ){?>
                            <br>
                            <div class="row">
                            <div class="col-xs-12">
                                <div id="Txt_ResultadoNeg" class="text-danger">Usuario o contraseña incorrectos, si no la recuerda contacte al administrador</div>
                            </div>
                            </div>
                        <?php }?>
                    </form>


                </div>
                <!-- /.login-box-body -->
            </div>
            <!-- /.login-box -->
        </div>
    </div>
    <!-- jQuery 3 -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- iCheck -->
    <script src="../plugins/iCheck/icheck.min.js"></script>
    <script>
        $(function() {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' /* optional */
            });
        });
    </script>
</body>

</html>