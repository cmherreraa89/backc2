<?php
$loc = "home";
include_once("src/mobiles.php");
require_once("src/seguridad.php");
?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <?php include_once("src/header.php"); ?>
</head>

<body class="hold-transition skin-green sidebar-mini">
<div class="wrapper">
  <?php include_once("src/main_header.php"); ?>
  

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Page Header
        <small>Optional description</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">

      <!--------------------------
        | Your Page Content Here |
        -------------------------->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include_once("src/main_footer.php"); ?>
  

</div>
<!-- ./wrapper -->
<?php include_once("src/footer.php"); ?>
</body>
</html>