<?php
$loc = "orde";
include_once("src/mobiles.php");
require_once("src/seguridad.php");
?>
<!DOCTYPE html>
<html>
<head>
  <?php include_once("src/header.php"); ?>
</head>
<body class="hold-transition skin-green sidebar-mini">
<div class="wrapper">
  <?php include_once("src/main_header.php"); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Ordenes de Trabajo
        <small>Módulo para la administración de ordenes de trabajo, aquí se almacenan o consulta la información de todas aquellas ordenes que se han emitido en los frentes de obra</small>
      </h1>
      <!--<ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
      </ol>-->
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
    <!-- Main content -->

    <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"><button type="button" class="btn btn-info btn-xs"><i class="fa fa-plus-square"></i> Agregar Orden</button></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Fecha</th>
                  <th>Equipo</th>
                  <th>Horario</th>
                  <th>Combustible</th>
                  <th>Locación</th>
                  <th>Observaciones</th>
                  <th>Aceptación</th>
                  <th>Estado</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                  <td style="text-align: center;">
                        0001
                  </td>
                  <td>01-04-2019<br>Lunes</td>
                  <td>Volqueta OBS-397<br>Jose Enriquez López</td>
                  <td>Horario: 08:00 - 17:00<br>Trabajo: 08:10 - 17:00<br>Descuento:1 hora<br>Horas: 8H</td>
                  <td>Diesel: 10 Gal<br>Diesel $: $87.500<br>Viajes: 6<br>Material: Arena</td>
                  <td>Remodelación Parque la Ceiba<br>Fuente: Puente Rio Meta<br>Destino: Obra parque Ceiba</td>
                  <td></td>
                  <td><i class="fa fa-user"></i> Diego A Lamprea M<br><i class="fa fa-user"></i><br><i class="fa fa-user"></i></td>
                    <td>
                        <button type="button" class="btn btn-success btn-xs"><i class="fa fa-edit"></i></button> 
                        <button type="button" class="btn btn-warning btn-xs"><i class="fa fa-briefcase"></i></button>
                    </td>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include_once("src/main_footer.php"); ?>
</div>
<!-- ./wrapper -->
<?php include_once("src/footer.php"); ?>
</body>
</html>