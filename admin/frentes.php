<?php
$loc = "fren";
include_once("src/mobiles.php");
require_once("src/seguridad.php");
?>
<!DOCTYPE html>
<html>
<head>
  <?php include_once("src/header.php"); ?>
  <style type="text/css" media="screen">
    @media (min-width: 992px) {
        .modal-lg {
            width: 1200px;
            height: 600px; /* control height here */
        }
    }

    .modal img {
          max-width: 25%;
          border-radius: 75px;
          /*border: 1px solid #aaa;*/
      }

      #imagen > p{
        color:#4ebfbb;
        cursor: pointer;
        font-weight: bold;
      }
  </style>
</head>

<body class="hold-transition skin-green sidebar-mini">
<div class="wrapper">
  <?php include_once("src/main_header.php"); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Frentes de Obra
        <small>Aquí se encontrará la información de los frentes de obra para los cuales se tiene registro de participación entre Grupo C2 y empresas aliadas, se puede consultar el histórico y demás.</small>
      </h1>
      <!--<ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
      </ol>-->
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
    <!-- Main content -->

    <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"><button id="crear" type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus-square"></i> Agregar Frente de Obra</button></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="table-trabajadores" class="table table-bordered table-striped display" style="width:100%">
                <thead>
                <tr>
                  <th>Imagen</th>
                  <th>Nombre</th>
                  <th>Contrato</th>
                  <th>Empresas</th>
                  <th>Localización</th>
                  <th>Coordenadas</th>
                  <th>Estado</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
                <!-- <tr>
                  <td style="text-align: center;">
                    <img src="../dist/img/frentes/FrenteCeiba.png" style="width: 100px;" alt="Remodelación Parque la Ceiba">
                  </td>
                  <td>Remodelación Parque la Ceiba<br>Horario 07:30 - 17:30</td>
                  <td>Contrato CP-010-2018</td>
                  <td>Grupo C2 SAS<br><span class="text-success">Inversiones Altos de Tocumen (Formato)</span></td>
                  <td>Puerto López | META | Colombia</td>
                  <td>4.084923, -72.952059</td>
                  <td><span class="text-success">Activo</span></td>
                  <td>
                    <button type="button" class="btn btn-success btn-xs"><i class="fa fa-edit"></i></button>
                    <button type="button" class="btn btn-warning btn-xs"><i class="fa fa-briefcase"></i></button>
                    <button type="button" class="btn btn-info btn-xs"><i class="fa fa-image"></i></button>
                  </td>
                </tr> -->
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Modal -->
      <div class="modal fade" id="myModal" role="dialog">
                      <div class="modal-dialog modal-lg">

                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" id="tipo-modal">Datos actuales del usuario</h4>
                          </div>
                          <!-- Inicio formulario -->
                                <form autocomplete="off" id="frm">
                                      <div class="modal-body">

                                              <div class="row">
                                                <div class="col-md-3"></div>
                                                <div class="col-md-6">
                                                      <center>
                                                          <div id="imagen">
                                                            <img id="photo" src="../dist/img/frentes/frente-profile.png" />
                                                            <input accept="image/*" name="file-photo" id="file-photo" type="file" style="display: none;" />
                                                            <p id="editar-imagen">Editar <i class="fa fa-edit"></i></p>
                                                          </div>
                                                        </center>
                                                </div>
                                                <div class="col-md-3"></div>
                                              </div>

                                              <div class="row">

                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                      <label for="nombre">Nombre</label>
                                                      <input type="text" class="form-control" id="nombre" name="nombre" placeholder="" autocomplete="off" required>
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                  <div class="form-group">
                                                    <label for="hora_inicio">Hora inicio</label>
                                                    <input type="text" class="form-control" id="hora_inicio" name="hora_inicio" placeholder="" autocomplete="off" required>
                                                  </div>
                                                </div>

                                                <div class="col-md-3">
                                                  <div class="form-group">
                                                    <label for="hora_fin">Hora fin</label>
                                                    <input type="text" class="form-control" id="hora_fin" name="hora_fin" placeholder="" autocomplete="off" required>
                                                  </div>
                                                </div>

                                                <div class="col-md-3">
                                                  <div class="form-group">
                                                    <label for="alm_inicio">Almuerzo incio</label>
                                                    <input type="text" class="form-control" id="alm_inicio" name="alm_inicio" placeholder="" autocomplete="off" required>
                                                  </div>
                                                </div>

                                              </div>

                                              <div class="row">
                                                <div class="col-md-3">
                                                  <div class="form-group">
                                                    <label for="alm_fin">Almuerzo Fin</label>
                                                    <input type="text" class="form-control" id="alm_fin" name="alm_fin" placeholder="" autocomplete="off" required>
                                                  </div>
                                                </div>

                                                <div class="col-md-3">
                                                  <div class="form-group">
                                                    <label for="contrato">Contrato</label>
                                                    <input type="text" class="form-control" id="contrato" name="contrato" placeholder="" autocomplete="off" required>
                                                  </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                      <label for="coordenadas">Coordenadas</label>
                                                      <input id="coordenadas" name="coordenadas" type="text" class="form-control" placeholder="" />
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <!-- select -->
                                                    <div class="form-group">
                                                      <label for="ciudad_id">Ciudad</label>
                                                      <select id="ciudad_id" name="ciudad_id" data-placeholder="Selecionar ciudad" class="select2 form-control" tabindex="-1" required>
                                                      </select>
                                                    </div>
                                                    <!-- select -->
                                                </div>
                                              </div>

                                              <!-- row -->
                                               <div class="row">

                                                <div class="col-md-3">
                                                    <!-- select -->
                                                      <div class="form-group">
                                                        <label for="estado_id">Estado</label>
                                                        <select id="estado_id" name="estado_id" data-placeholder="Selecionar Estado" class="select2 form-control" tabindex="-1" required>
                                                        </select>
                                                      </div>
                                                      <!-- select -->
                                                </div>
                                                <div class="col-md-9">
                                                </div>
                                              </div>
                                              <!-- ./row -->
                                      </div>
                                      <div class="modal-footer">
                                        <button id="enviar-datos" type="submit" class="btn btn-info">Actualizar</button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                      </div>

                                </form>
                                <!-- Fin formulario -->
                          <!-- End Modal content-->
                        </div>
                      </div>
                    </div>
        <!--/ Modal -->

        <!-- Modal_archivos -->
                      <div class="modal fade" id="Modal_archivos" role="dialog">
                        <div class="modal-dialog">

                          <!-- Modal content-->
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                              <h4 class="modal-title" id="tipo-modal">Archivos de la empresa</h4>
                            </div>
                            <div class="modal-body">

                              <!-- Inicio formulario -->
                                  <form autocomplete="off" id="frm_archivo">

                                    <div class="row">
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="nombre">Nombre archivo</label>
                                          <input type="text" class="form-control" id="nombre" name="nombre" autocomplete="off" required>
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                          <div class="form-group">
                                            <label for="archivo">Seleccionar archivo</label>
                                            <input type="file" class="form-control" id="archivo" name="archivo" accept="application/pdf" autocomplete="off" required>
                                          </div>
                                      </div>
                                    </div>

                                    <button type="submit" class="btn btn-info">Agregar</button>
                                  </form>
                                  <!-- Fin formulario -->

                                  <!-- Inicio table -->
                                    <h2>Archivos de esta empresa</h2>
                                    <!-- <p>The .table class adds basic styling (light padding and only horizontal dividers) to a table:</p> -->
                                    <div class="table-responsive">
                                    <table id="table_archivos" class="table">
                                      <thead>
                                        <tr>
                                          <th>Nombre</th>
                                          <th>Acciones</th>
                                        </tr>
                                      </thead>
                                      <tbody>

                                      </tbody>
                                    </table>
                                  </div>
                              <!-- fin tabla -->

                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            </div>

                            <!-- End Modal content-->
                          </div>
                        </div>

                       </div>
                       <!--Fin  Modal archivos -->

                       <!-- Modal_ aliados -->
                      <div class="modal fade" id="Modal_aliados" role="dialog">
                        <div class="modal-dialog">

                          <!-- Modal content-->
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                              <h4 class="modal-title" id="tipo-modal">Empresas aliadas</h4>
                            </div>
                            <div class="modal-body">

                              <!-- Inicio formulario -->
                                  <form autocomplete="off" id="frm_aliados">

                                    <div class="row">
                                      <div class="col-md-6">
                                        <!-- select -->
                                          <div class="form-group">
                                            <label for="empresa_id">Empresa</label>
                                            <select id="empresa_id" name="empresa_id" data-placeholder="Seleccionar empresa" class="select2 form-control" tabindex="-1" required>
                                            </select>
                                          </div>
                                          <!-- select -->
                                      </div>
                                      <div class="col-md-6">
                                          <!-- select -->
                                            <div class="form-group">
                                              <label for="tiposociedad">Sociedad</label>
                                              <select id="tiposociedad" name="tiposociedad" data-placeholder="Seleccionar tipo sociedad" class="select2 form-control" tabindex="-1" required>
                                                <option value=""></option>
                                                <option value="1">Principal</option>
                                                <option value="2">Secundaria</option>
                                              </select>
                                            </div>
                                            <!-- select -->
                                      </div>
                                    </div>

                                    <button type="submit" class="btn btn-info btn-xs">Agregar</button>
                                  </form>
                                  <!-- Fin formulario -->

                                  <!-- Inicio table -->
                                    <h2>Empresas aliadas</h2>
                                    <!-- <p>The .table class adds basic styling (light padding and only horizontal dividers) to a table:</p> -->
                                    <div class="table-responsive">
                                    <table id="table_aliados" class="table">
                                      <thead>
                                        <tr>
                                          <th>Nombre</th>
                                          <th>Tipo de sociedad</th>
                                          <th>Acciones</th>
                                        </tr>
                                      </thead>
                                      <tbody>

                                      </tbody>
                                    </table>
                                  </div>
                              <!-- fin tabla -->

                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            </div>

                            <!-- End Modal content-->
                          </div>
                        </div>

                       </div>
                       <!--Fin  Modal archivos -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include_once("src/main_footer.php"); ?>
</div>
<!-- ./wrapper -->
<?php include_once("src/footer.php"); ?>
<script src="js/frente.js?v=<?php echo $sin_cache ?>"></script>
</body>
</html>