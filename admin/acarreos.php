<?php
$loc = "acar";
include_once("src/mobiles.php");
require_once("src/seguridad.php");
?>
<!DOCTYPE html>
<html>
<head>
  <?php include_once("src/header.php"); ?>
</head>
<body class="hold-transition skin-green sidebar-mini">
<div class="wrapper">
  <?php include_once("src/main_header.php"); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Acarreos
        <small>Módulo para la administración de ordenes de trabajo, aquí se almacenan o consulta la información de todas aquellas ordenes que se han emitido en los frentes de obra</small>
      </h1>
      <!--<ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
      </ol>-->
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
    <!-- Main content -->

    <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"><button type="button" class="btn btn-info btn-xs"><i class="fa fa-plus-square"></i> Agregar Acarreo</button></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Placa</th>
                  <th>Chofer</th>
                  <th>Pala</th>
                  <th>Cubicaje</th>
                  <th>Horario</th>
                  <th>Viajes</th>
                  <th>M<sup>3</sup></th>
                  <th>Observaciones</th>
                  <th>Estado</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                  <td style="text-align: center;">
                        1
                  </td>
                  <td>OBS-397</td>
                  <td>Jose Enriquez López</td>
                  <td></td>
                  <td></td>
                  <td>Inicio: 08:00 | Fin 17:00<br>Horas: 9H</td>
                  <td>6 Viajes</td>
                  <td>18 m<sup>3</sup></td>
                  <td></td>
                    <td>
                        <button type="button" class="btn btn-success btn-xs"><i class="fa fa-edit"></i></button> 
                        <button type="button" class="btn btn-warning btn-xs"><i class="fa fa-briefcase"></i></button>
                    </td>
                </tr>
                
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include_once("src/main_footer.php"); ?>
</div>
<!-- ./wrapper -->
<?php include_once("src/footer.php"); ?>
</body>
</html>