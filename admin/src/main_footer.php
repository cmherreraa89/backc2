<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      Grupo C2 | PMOC (Plataforma de Mantenimiento en Operaciones Civiles)
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; <?php echo(date("Y")); ?> <a href="http://www.luduscolombia.com.co/" target="_blank">Ludus Colombia SAS</a>.</strong> Todos los derechos reservados
  </footer>