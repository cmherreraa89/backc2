<?php

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");


$sin_cache = md5( time() );
ini_set("session.cookie_lifetime","107200");
ini_set("session.gc_maxlifetime","107200");
@session_start();

// Juan Carlos villar, seguridad para las peticiones de la app
if( isset( $_POST['token'] ) ){
      include_once("../models/login.php");
      $valida_token = IngresoUsuario::validar_token( $_POST );
      if ( $valida_token ) {
          //echo "Token ok";
          $_POST['opcn'] = $_GET['opcn'];
          @session_start();
          $usuario_id = IngresoUsuario::id_usuario( $_POST['token'] );
          $_SESSION['user']['usuario_id'] = $usuario_id;
      }else{
          echo "Token no autorizado";
          die();
      }
  }
// Fin seguridad app

if(!isset($_SESSION['user']['apellidos'])){
  if( isset($_COOKIE['cookieGrupoC2']) ){
    include_once("models/login.php");
    $p['cookieGrupoC2'] = $_COOKIE['cookieGrupoC2'];
    $p['deviceType']    = $deviceType;
    $p['versionEquipo'] = $versionEquipo;
    $usuario = IngresoUsuario::ValidaCookUsuario( $p, '' );
    if($usuario['usuario_id']>0){
      @session_start();
      $_SESSION['user'] = $usuario;
      $loca = $_COOKIE['UrlActGrupoC2'];
      //header("location: ");
    }
  }else{
    header("location: login");
  }
}else{

  if( isset( $_GET['opcn']) ){
    $_POST['opcn'] = $_GET['opcn'];
  }

}