<!-- Main Header -->
<header class="main-header">
    <!-- Logo -->
    <a href="home" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>G</b>C2</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Grupo</b>C2</span>
    </a>
    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <!-- Notifications Menu -->
          <li class="dropdown notifications-menu">
            <!-- Menu toggle button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning">10</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 10 notifications</li>
              <li>
                <!-- Inner Menu: contains the notifications -->
                <ul class="menu">
                  <li><!-- start notification -->
                    <a href="#">
                      <i class="fa fa-users text-aqua"></i> 5 new members joined today
                    </a>
                  </li>
                  <!-- end notification -->
                </ul>
              </li>
              <li class="footer"><a href="#">View all</a></li>
            </ul>
          </li>
          <!-- User Account Menu -->
          <li class="dropdown user user-menu">
            <!-- Menu Toggle Button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <!-- The user image in the navbar-->
              <img src="../dist/img/usuarios/<?php echo($_SESSION['user']['fotografia']);?>" class="user-image" alt="User Image">
              <!-- hidden-xs hides the username on small devices so only the image appears. -->
              <span class="hidden-xs"><?php echo($_SESSION['user']['nombres']);?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- The user image in the menu -->
              <li class="user-header">
                <img src="../dist/img/usuarios/<?php echo($_SESSION['user']['fotografia']);?>" class="img-circle" alt="User Image">
                <p>
                    <?php echo($_SESSION['user']['nombres'].' '.$_SESSION['user']['apellidos']);?><br>
                  <small class="text-inverse"><?php echo($_SESSION['user']['cargo']);?></small>
                  <small>Ingresó: <?php echo($_SESSION['user']['fecha_ingreso']);?></small>
                </p>
              </li>
              <!-- Menu Body -->
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="perfil.php" class="btn btn-default btn-flat">Perfil</a>
                </div>
                <div class="pull-right">
                  <a href="salir" class="btn btn-default btn-flat">Salir</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <!--<li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>-->
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="../dist/img/usuarios/<?php echo($_SESSION['user']['fotografia']);?>" class="img-circle" alt="<?php echo($_SESSION['user']['nombres']);?>">
        </div>
        <div class="pull-left info">
          <p><?php echo($_SESSION['user']['nombres']);?></p>
          <!-- Status -->
          <a href="#"><i class="fa fa-circle text-success"></i> En línea</a>
        </div>
      </div>

      <!-- search form (Optional) -->
      <!--<form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
              <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
              </button>
            </span>
        </div>
      </form>-->
      <!-- /.search form -->

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MENÚ</li>
        <!-- Optionally, you can add icons to the links -->
        <li <?php if(isset($loc)&&$loc=="home"){?>class="active"<?php }?>><a href="home"><i class="fa fa-home"></i> <span>Inicio</span></a></li>
        <li <?php if(isset($loc)&&$loc=="empr"){?>class="active"<?php }?>><a href="empresas"><i class="fa fa-industry"></i> <span>Empresas Aliadas</span></a></li>
        <li <?php if(isset($loc)&&$loc=="fren"){?>class="active"<?php }?>><a href="frentes"><i class="fa fa-road"></i> <span>Frentes de Obra</span></a></li>
        <li <?php if(isset($loc)&&$loc=="maqu"){?>class="active"<?php }?>><a href="maquinarias"><i class="fa fa-truck"></i> <span>Maquinaria</span></a></li>
        <li <?php if(isset($loc)&&$loc=="trab"){?>class="active"<?php }?>><a href="trabajadores"><i class="fa fa-users"></i> <span>Personal</span></a></li>
        <li class="treeview <?php if(isset($loc)&&($loc=="orde"||$loc=="acar"||$loc=="hora")){?>active menu-open<?php }?>">
          <a href="#"><i class="fa fa-dashboard"></i> <span>Operación</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu" <?php if(isset($loc)&&($loc=="orde"||$loc=="acar"||$loc=="hora")){?>style="display:block;"<?php }?>>
            <li <?php if(isset($loc)&&$loc=="orde"){?>class="active"<?php }?>><a href="ordenes"><i class="fa fa-cloud-upload"></i> <span>Ordenes de trabajo</span></a></li>
            <li <?php if(isset($loc)&&$loc=="acar"){?>class="active"<?php }?>><a href="acarreos"><i class="fa fa-eye"></i> <span>Control de Acarreo</span></a></li>
            <li <?php if(isset($loc)&&$loc=="hora"){?>class="active"<?php }?>><a href="horarios"><i class="fa fa-hourglass-half"></i> <span>Control de Horario</span></a></li>
            <li><a href="#"><i class="fa fa-wrench"></i> <span>Mantenimiento</span></a></li>
            <li><a href="#"><i class="fa fa-cubes"></i> <span>Inventario Materiales</span></a></li>
          </ul>
        </li>
        <li class="treeview <?php if(isset($loc)&&($loc=="perfil")){?>active menu-open<?php }?>">
          <a href="#"><i class="fa fa-cog"></i> <span>Configuración</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu" <?php if(isset($loc)&&($loc=="perfil")){?>style="display:block;"<?php }?>>
            <li <?php if(isset($loc)&&$loc=="perfil"){?>class="active"<?php }?>><a href="perfil"><i class="fa fa-user-secret"></i> <span>Perfil</span></a></li>
            <li><a href="#"><i class="fa fa-briefcase"></i> <span>Cargos</span></a></li>
            <li><a href="#"><i class="fa fa-filter"></i> <span>Estándares Maquinaria</span></a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#"><i class="fa fa-bar-chart"></i> <span>Reportes</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="perfil"><i class="fa fa-user-secret"></i> <span>Nómina</span></a></li>
            <li><a href="#"><i class="fa fa-area-chart"></i> <span>Uso Materiales</span></a></li>
            <li><a href="#"><i class="fa fa-road"></i> <span>Frente de Obra</span></a></li>
          </ul>
        </li><br><br><br>
        <li><a href="salir"><i class="fa fa-power-off"></i> <span>Salir</span></a></li>
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>