<?php
class IngresoUsuario {
    public static function ingresarUsuario( $p, $prof = '../' ){
        include_once( $prof.'../config/init_db.php' );
        $password   = "AES_ENCRYPT(MD5('".$_POST['password']."'),'_GrupoC2_2019*')";
        try {
            $account = DB::queryFirstRow("SELECT * FROM dl_usuario WHERE identificacion=%s AND contrasena=$password AND estado_id=1", $p['identificacion']  );
            if( isset($account['usuario_id']) && $account['usuario_id']>0 ){
                $ip_dir = $_SERVER['REMOTE_ADDR'];
                DB::insert( 'dl_registro_acceso', [
                    'id_usuario'        => $account['usuario_id'],
                    'ip_dir'            => $ip_dir,
                    'equipo'            => $p['deviceType'],
                    'version'           => $p['versionEquipo'],
                    'fecha_creacion'    => DB::sqleval("NOW()")
                ] );
                //return DB::insertId();
                return $account;
            }else{
                return 0;
            }
        } catch(MeekroDBException $e) {
            return 0;
        }
    }
    public static function ValidaCookUsuario( $p, $prof = '../' ){
        include_once( $prof.'../config/init_db.php' );
        $id_usrCook = $p['cookieGrupoC2'];
        try {
            $account = DB::queryFirstRow("SELECT * FROM dl_usuario WHERE MD5(CONCAT(usuario_id, '', 'GrupoC22019')) = '$id_usrCook' AND estado_id=1");
            if( isset($account['usuario_id']) && $account['usuario_id']>0 ){
                $ip_dir = $_SERVER['REMOTE_ADDR'];
                DB::insert( 'dl_registro_acceso', [
                    'id_usuario'        => $account['usuario_id'],
                    'ip_dir'            => $ip_dir,
                    'equipo'            => $p['deviceType'],
                    'version'           => $p['versionEquipo'],
                    'fecha_creacion'    => DB::sqleval("NOW()")
                ] );
                //return DB::insertId();
                return $account;
            }else{
                return 0;
            }
        } catch(MeekroDBException $e) {
            return 0;
        }
    }


    public static function ingresarUsuario_app( $p ){
        include_once( '../../config/init_db.php' );
        $password   = "AES_ENCRYPT(MD5('".$_POST['password']."'),'_GrupoC2_2019*')";
        try {
            $account = DB::query("SELECT * FROM dl_usuario WHERE identificacion=%s AND contrasena=$password AND estado_id=1", $p['identificacion']  );
            if( count( $account ) > 0 ){

                $ip_dir =  isset( $_SERVER['REMOTE_ADDR'] ) ? $_SERVER['REMOTE_ADDR'] : '' ;
                $insertar = DB::insert( 'dl_registro_acceso', [
                    'id_usuario'        => $account[0]['usuario_id'],
                    'ip_dir'            => $ip_dir,
                    'equipo'            => 'celular',
                    'version'           => 'version',
                    'fecha_creacion'    => DB::sqleval("NOW()")
                ] );

                //$token = md5($account[0]['usuario_id'].time().$account[0]['email'].time()) ;
                $token = $rand_part = str_shuffle("0123456789abcdefghijklmnopq0123456789rstuvwxyz0123456789".uniqid());
                $query_update = "UPDATE dl_usuario set token = '$token' where usuario_id = '{$account[0]['usuario_id']}'";
                $actualizar = DB::query( $query_update );
                unset($account[0]['contrasena']);
                $account[0]['token'] = $token;
                return $account[0];
            }else{
                return 0;
            }
        } catch(MeekroDBException $e) {
            return 0;
        }
    }


    public static function validar_token( $p ){
        include_once( '../../config/init_db.php' );

        $query_token = "SELECT * FROM dl_usuario where token = '{$p['token']}'";
        $token = DB::query( $query_token );
        if ( count($token) > 0) {
            return true;
        }else{
            return false;
        }

    }

    public static function id_usuario( $token ){
        include_once( '../../config/init_db.php' );
        $query_usuario_id = "SELECT usuario_id FROM dl_usuario where token = '$token'";
        $usuario_id = DB::query( $query_usuario_id );
        return $usuario_id[0]['usuario_id'];
    }

}

