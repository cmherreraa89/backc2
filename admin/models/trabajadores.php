<?php
Class Trabajadores {

  /*Obtener los usuarios*/
  public static function select_Users( $p ){
	    include_once( '../../config/init_db.php' );
	    DB::$encoding = 'utf8'; // defaults to latin1 if omitted

		$like           =   $p['search']['value']; // lo que se va a buscar en los campos
	    $limit          =   'limit '.$p['start'].','.$p['length'];  //limite a mostrar en la datatable
	    $Aux_order_by   =   $p['order'][0]['column']; // Campo por el qeu se va a ordenar la datatable
	    $asc_desc       =   $p['order'][0]['dir'];  // condicion por la qeu se va a ordenar el campo, desc asc

	    $order_by = ' order by ';

	    switch ( $Aux_order_by ) {
	    	case '0':
	    		$order_by .= 'nombres ';
	    		break;
	    	case '1':
	    		$order_by .= 'identification ';
	    		break;
	    	case '2':
	    		$order_by .= 'email ';
	    		break;
	    	case '3':
	    		$order_by .= 'phone ';
	    		break;
	    	case '4':
	    		$order_by .= 'address ';
	    		break;
	    	case '5':
	    		$order_by .= 'city ';
	    		break;
	    	case '6':
	    		$order_by .= 'profile ';
	    		break;
	    	case '7':
	    		$order_by .= 'headquarter ';
	    		break;
	    	case '8':
	    		$order_by .= 'first_name ';
	    		break;

	    }

	    $order_by .= $asc_desc;
	    $where     = " WHERE u.nombres like '%$like%' or u.apellidos like '%$like%' or u.identificacion like '%$like%' or u.email like '%$like%' ";

	    if( strpos($like, "+") ){
				$array = explode("+", $like);
					foreach ($array as $key => $value) {
						if( $key == 0){
								$where .= " or (u.nombres like '%$value%' or u.apellidos like '%$value%' or u.identificacion like '%$value%' or u.email like '%$value%') ";
						}else{
								$where .= " and (u.nombres like '%$value%' or u.apellidos like '%$value%' or u.identificacion like '%$value%' or u.email like '%$value%') ";
						}

					}
			}

	    $queryPer = "SELECT u.usuario_id, u.nombres, u.apellidos, u.fotografia, u.identificacion, u.email, u.direccion, u.telefono, u.celular, u.cumpleanos, u.fecha_ingreso, u.cargo, u.estado_id,
						ti.tipo_identificacion, ci.ciudad, e.eps, ar.arl, af.afp, cc.ccf, es.estado, em.empresa
						FROM dl_usuario u
								inner join dl_tipo_identificacion ti
										on ti.tipo_identificacion_id = u.tipo_identificacion_id
								inner join dl_ciudad ci
										on ci.ciudad_id = u.ciudad_id
								inner join dl_eps e
										on e.eps_id = u.eps_id
								inner join dl_arl ar
										on ar.arl_id = u.arl_id
								inner join dl_afp af
										on af.afp_id = u.afp_id
								inner join dl_ccf cc
										on cc.ccf_id = u.ccf_id
								inner join dl_estado es
										on es.estado_id = u.estado_id
								inner join dl_empresa em
										on em.empresa_id = u.empresa_id
								$where
	    						$order_by ";
		$encontrados_total = DB::query( $queryPer );

		$encontrados = DB::query( $queryPer.$limit );

		foreach ($encontrados_total as $key => $value) {

			$encontrados[$key]['bienestar'] = '<span class="small">ARL: '.$value['arl'].'<br>
								                      EPS: '.$value['eps'].'<br>
								                      AFP: '.$value['afp'].'<br>
								                      CCF: '.$value['ccf'].'
								               </span>';

			$encontrados[$key]['contacto'] = '<span class="small">Dirección: '.$value['direccion'].'<br>
							                    Teléfono: '.$value['telefono'].'<br>
							                    Celular: '.$value['celular'].'<br>
							                    E-mail: '.$value['email'].'
							                    </span>';

			if( $value['estado_id'] == 1){
				$encontrados[$key]['estado'] = '<span class="text-success">'.$value['estado'].'</span>';
			}else if( $value['estado_id'] == 2){
				$encontrados[$key]['estado'] = '<span class="text-danger">'.$value['estado'].'</span>';
			}

				$encontrados[$key]['acciones'] = '<div data-id='.$value['usuario_id'].'>
										            <button type="button" class="editar btn btn-success btn-xs" data-toggle="modal" data-target="#myModal"><i class="fa fa-edit"></i></button>
										            <hr style="margin: 3px;">
										            <button type="button" class="addArchivo btn btn-warning btn-xs" data-toggle="modal" data-target="#Modal_archivos"><i class="fa fa-briefcase"></i></button>
										        </div>';

				$encontrados[$key]['fotografia'] = '<img src="../dist/img/usuarios/'.$value['fotografia'].'" style="height: 80px;" alt="Grupo C2 SAS">';

		}


		$queryTotal = "SELECT count(*) as cantidad FROM dl_usuario";
		$counter 	= DB::query( $queryTotal );
		$counter	= $counter[0]['cantidad'];

		$datos = array();
		$datos['draw']              = $p['draw'];    // Consecutivo
	    $datos['recordsTotal']      = $counter;   // Total de regisrtos encontrados
	    $datos['recordsFiltered']   = count( $encontrados_total );    // Cantidad de resgistros encontrados
	    $datos['data']              = $encontrados; // datos encontrados

    return $datos;
  }

  /*Obtener los datos iniciales*/
	  public static function select_init(){
	    include_once( '../../config/init_db.php' );
	    DB::$encoding = 'utf8'; // defaults to latin1 if omitted

	    $queryEPS 	= "SELECT eps_id, eps FROM dl_eps where estado_id = 1;";
	    $resultSet_eps = DB::query( $queryEPS );

	    $queryARL 	= "SELECT arl_id, arl FROM dl_arl where estado_id = 1;";
	    $resultSet_arl = DB::query( $queryARL );

	    $queryCaja 	= "SELECT ccf_id, ccf FROM dl_ccf where id_estado = 1;";
	    $resultSet_caja = DB::query( $queryCaja );

	    $queryTi 	= "SELECT tipo_identificacion_id, tipo_identificacion FROM dl_tipo_identificacion;";
	    $resultSet_ti = DB::query( $queryTi );

	    $queryEmp 	= "SELECT empresa_id, empresa FROM dl_empresa where estado_id = 1;";
	    $resultSet_Emp = DB::query( $queryEmp );

	    $queryCiu = "SELECT ciudad_id, ciudad FROM dl_ciudad where estado_id = 1 order by ciudad";
		$resultSet_ciu = DB::query( $queryCiu );

		$queryEst = "SELECT * FROM dl_estado;";
		$resultSet_est = DB::query( $queryEst );

		$queryAfp = "SELECT afp_id, afp FROM dl_afp;";
		$resultSet_afp = DB::query( $queryAfp );

		$querybanco = "SELECT * FROM dl_bancos;";
		$resultSet_banco = DB::query( $querybanco );


	    $datos['eps']	= $resultSet_eps;
	    $datos['arl']	= $resultSet_arl;
	    $datos['caja']	= $resultSet_caja;
	    $datos['ti']	= $resultSet_ti;
	    $datos['emp']	= $resultSet_Emp;
	    $datos['ciu']	= $resultSet_ciu;
	    $datos['est']	= $resultSet_est;
	    $datos['afp']	= $resultSet_afp;
	    $datos['banco']	= $resultSet_banco;

	    return $datos;
	  }

	// Obtener datos de un usuario
	public static function consultar_usuario( $id ){

	    include_once( '../../config/init_db.php' );
	    $query = "SELECT u.usuario_id, u.nombres, u.apellidos, u.fotografia, u.identificacion, u.email, u.direccion, u.telefono, u.celular, u.cumpleanos, u.fecha_ingreso, u.cargo, u.estado_id, u.nacimiento_ciudad_id, u.exp_ciudad_id, u.fecha_expedicion, u.cumpleanos, u.barrio, u.nomina,u.arl_tarifa,
	    	u.sueldo_real, u.bonificacion,u.tipo_bonificacion,u.tipo_cuenta, u.banco_id, u.cuenta,u.fecha_retiro, u.genero,
						u.tipo_identificacion_id, u.ciudad_id, u.eps_id, u.arl_id, u.afp_id, u.ccf_id, u.estado_id, u.empresa_id, u.perfil_id
						FROM dl_usuario u where u.usuario_id = $id";
		$resultSet = DB::query( $query );
	    return $resultSet[0];
	  }

	  // Obtener datos de un usuario
	public static function consultar_perfil_usuario( $id ){

	    include_once( '../../config/init_db.php' );
	    $query = "SELECT u.usuario_id, u.nombres, u.apellidos, u.fotografia, u.identificacion, u.email, u.direccion, u.telefono, u.celular, u.cumpleanos, u.fecha_ingreso, u.cargo, u.estado_id, ti.tipo_identificacion, ci.ciudad, e.eps, ar.arl, af.afp, cc.ccf, es.estado, em.empresa
						FROM dl_usuario u
								inner join dl_tipo_identificacion ti
										on ti.tipo_identificacion_id = u.tipo_identificacion_id
								inner join dl_ciudad ci
										on ci.ciudad_id = u.ciudad_id
								inner join dl_eps e
										on e.eps_id = u.eps_id
								inner join dl_arl ar
										on ar.arl_id = u.arl_id
								inner join dl_afp af
										on af.afp_id = u.afp_id
								inner join dl_ccf cc
										on cc.ccf_id = u.ccf_id
								inner join dl_estado es
										on es.estado_id = u.estado_id
								inner join dl_empresa em
										on em.empresa_id = u.empresa_id
								where usuario_id = $id";
		$resultSet = DB::query( $query );
	    return $resultSet[0];
	  }

  /*Actualizar los usuarios*/
  public static function update_Usuarios( $p ){
    include_once( '../../config/init_db.php' );

    $queryPer = "UPDATE dl_usuario
							SET
							nombres 				= '{$p['nombres']}',
							apellidos 				= '{$p['apellidos']}',
							fotografia 				= '{$p['fotografia']}',
							tipo_identificacion_id 	= '{$p['tipo_identificacion_id']}',
							identificacion 			= '{$p['identificacion']}',
							exp_ciudad_id			= '{$p['ciudad_exp']}',
							fecha_expedicion		= '{$p['fecha_expedicion']}',
							nacimiento_ciudad_id	= '{$p['ciudad_nacimiento']}',
							email 					= '{$p['email']}',
							direccion 				= '{$p['direccion']}',
							barrio 			   		= '{$p['barrio']}',
							telefono 				= '{$p['telefono']}',
							celular 				= '{$p['celular']}',
							ciudad_id 				= '{$p['ciudad_id']}',
							nomina					= '{$p['nomina']}',
							eps_id 					= '{$p['eps_id']}',
							arl_id 					= '{$p['arl_id']}',
							afp_id					= '{$p['arl_id']}',
							ccf_id 					= '{$p['ccf_id']}',
							arl_tarifa				= '{$p['tarifa_arl']}',
							sueldo_real				= '{$p['sueldo_real']}',		
							bonificacion            = '{$p['bonificacion']}',
							tipo_bonificacion		= '{$p['tipo_bonificacion']}',
							tipo_cuenta				= '{$p['tipo_cuenta']}',
							banco_id				= '{$p['banco']}',
							cuenta   				= '{$p['numero_cuenta']}',
							editor 					= {$_SESSION['user']['usuario_id']},
							edicion 				= now(),
							estado_id 				= '{$p['estado_id']}',
							cumpleanos				= '{$p['fecha_nacimiento']}',
							fecha_ingreso 			= '{$p['fecha_ingreso']}',
							fecha_retiro			= '{$p['fecha_retiro']}',
							empresa_id 				= '{$p['empresa_id']}',
							cargo 					= '{$p['cargo']}',
							perfil_id 				= '{$p['perfil_id']}',
							genero 					= '{$p['genero']}'
							WHERE usuario_id 		= '{$p['usuario_id']}';
							";
		$resultSet_usr = DB::query( $queryPer );

		$respuesta = array();
		if( $resultSet_usr ){
			$respuesta['error'] = false;
			$respuesta['msj'] 	= 'Usuario actualizado correctamente';

			$query_photo 		= "SELECT fotografia FROM dl_usuario where usuario_id = {$p['usuario_id']};";
			$resultSet_photo  	= DB::query( $query_photo );
			$photo 				= $resultSet_photo[0]['fotografia'];

			$_SESSION['user']['fotografia'] = $photo;
			$respuesta['photo'] = $photo;
		}else{
			$respuesta['error'] = true;
			$respuesta['msj'] 	= 'No se pudo actualizar el usuario';
		}
	    return json_encode($respuesta);
  }

  /*Crear usuarios*/
  public static function insert_Usuarios( $p, $prof = "../" ){
    include_once($prof.'../config/init_db.php');

    $query = "SELECT email FROM dl_usuario where email = '{$p['email']}'";
		$resultSet_email = DB::query( $query );

    	$respuesta = array();
		if( count( $resultSet_email ) > 0 ){
			$respuesta['error'] = true;
			$respuesta['msj'] 	= 'Correo ya registrado';
			return json_encode($respuesta);
		}


    $pass 	= $p['identificacion'];
    $key 	= Datos::$datos['key'];

    $password   = "AES_ENCRYPT(MD5('".$p['identificacion']."'),'".$key."')";



    $queryPer = "INSERT INTO dl_usuario
									(
									nombres,
									apellidos,
									fotografia,
									tipo_identificacion_id,
									identificacion,
									exp_ciudad_id,
									fecha_expedicion,
									nacimiento_ciudad_id,
									email,
									contrasena,
									direccion,
									barrio,
									telefono,
									celular,
									ciudad_id,
									nomina,
									eps_id,
									arl_id,
									afp_id,
									ccf_id,
									arl_tarifa,
									sueldo_real,
									bonificacion,
									tipo_bonificacion,
									tipo_cuenta,
									banco_id,
									cuenta,
									editor,
									edicion,
									estado_id,
									cumpleanos,
									fecha_ingreso,
									fecha_retiro,
									empresa_id,
									cargo,
									perfil_id,
									genero
									)
									VALUES
									(
									'{$p['nombres']}',
									'{$p['apellidos']}',
									'{$p['fotografia']}',
									'{$p['tipo_identificacion_id']}',
									'{$p['identificacion']}',
									'{$p['ciudad_exp']}',
									'{$p['fecha_expedicion']}',
									'{$p['ciudad_nacimiento']}',
									'{$p['email']}',
									 $password,
									'{$p['direccion']}',
									'{$p['barrio']}',
									'{$p['telefono']}',
									'{$p['celular']}',
									'{$p['ciudad_id']}',
									'{$p['nomina']}',
									'{$p['eps_id']}',
									'{$p['arl_id']}',
									'{$p['arl_id']}',
									'{$p['ccf_id']}',
									'{$p['tarifa_arl']}',
									'{$p['sueldo_real']}',	
									'{$p['bonificacion']}',
									'{$p['tipo_bonificacion']}',
									'{$p['tipo_cuenta']}',
									'{$p['banco']}',
									'{$p['numero_cuenta']}',
									{$_SESSION['user']['usuario_id']},
									now(),
									'{$p['estado_id']}',
									'{$p['fecha_nacimiento']}',
									'{$p['fecha_ingreso']}',
									'{$p['fecha_retiro']}',
									'{$p['empresa_id']}',
									'{$p['cargo']}',
									'{$p['perfil_id']}',
									'{$p['genero']}'

									);
									";
		$resultSet_usr = DB::query( $queryPer );

		$respuesta = array();
		if( $resultSet_usr ){
			$respuesta['error'] = false;
			$respuesta['msj'] = 'Usuario creado correctamente';
		}else{
			$respuesta['error'] = true;
			$respuesta['msj'] = 'No se pudo crear el usuario';
		}
	    return json_encode($respuesta);
  }

/*Seleccionar un imagen de una empresa*/
  public static function select_img_ant( $p ){
    include_once( '../../config/init_db.php' );

	$queryPer = "SELECT fotografia FROM dl_usuario
					where usuario_id = {$p['usuario_id']};";
		$resultSet = DB::query( $queryPer );
	    return $resultSet[0]['fotografia'];
  }

  /*Inicio metodos archivos usuarios*/

  /*Buscar archivos de las empresas*/
  public static function select_archivos( $p ){
    include_once( '../../config/init_db.php' );

	$queryPer = "SELECT archivousuario_id, archivo, nombre FROM dl_archivousuario
					where usuario_id = {$p['usuario_id']};";
		$resultSet = DB::query( $queryPer );

	    return json_encode($resultSet);
  }

  /*Seleccionar un archivo de una empresa*/
  public static function select_un_archivo( $p ){
    include_once( '../../config/init_db.php' );

	$queryPer = "SELECT archivo FROM dl_archivousuario
					where archivousuario_id = {$p['archivousuario_id']};";
		$resultSet = DB::query( $queryPer );
	    return $resultSet[0]['archivo'];
  }


  /*Borrar archivos de un usuario*/
  public static function borrar_archivo_usuario( $archivousuario_id ){
    include_once( '../../config/init_db.php' );

    	$query = "SELECT usuario_id FROM dl_archivousuario
					where archivousuario_id = $archivousuario_id;";
				$resultSet_emp = DB::query( $query );

		$queryPer = "DELETE FROM dl_archivousuario WHERE archivousuario_id = $archivousuario_id;";
		$resultSet = DB::query( $queryPer );

		$respuesta = array();
		if( $resultSet ){
			$respuesta['error'] = false;
			$respuesta['msj'] = 'Archivo borrado correctamente';
			$respuesta['usuario_id'] = $resultSet_emp[0]['usuario_id'];
		}else{
			$respuesta['error'] = true;
			$respuesta['msj'] = 'No se pudo borrar el archivo';
		}
	    return $respuesta;
  }


  /*Agregar archivo a una empresa*/
  public static function insert_Archivo( $p ){
    include_once( '../../config/init_db.php' );

	$query = "INSERT INTO dl_archivousuario
							(
							usuario_id,
							archivo,
							nombre
							)
							VALUES
							(
							'{$p['usuario_id']}',
							'{$p['archivo']}',
							'{$p['nombre']}'
							);
							";
		$resultSet = DB::query( $query );

		$respuesta = array();
		if( $resultSet ){
			$respuesta['error'] = false;
			$respuesta['msj'] = 'Archivo agregado correctamente';
			$respuesta['usuario_id'] = $p['usuario_id'];
		}else{
			$respuesta['error'] = true;
			$respuesta['msj'] = 'No se pudo agregar archivo a la empresa';
		}

	    return json_encode($respuesta);
  }

  /*Fin metodos archivos usuarios*/


}
