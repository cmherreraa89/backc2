<?php
Class Perfil {

  /*Obtener los datos iniciales*/
	  public static function select_init(){
	    include_once( '../../config/init_db.php' );
	    DB::$encoding = 'utf8'; // defaults to latin1 if omitted

	    $queryUsr 	= "SELECT u.usuario_id, u.nombres, u.apellidos, u.fotografia, u.identificacion, u.email, u.direccion, u.telefono, u.celular, u.cumpleanos, u.fecha_ingreso, u.cargo, u.estado_id,
						ti.tipo_identificacion, ci.ciudad, e.eps, ar.arl, af.afp, cc.ccf, es.estado, em.empresa,
						CASE
						    WHEN u.perfil_id = 1 THEN 'Administrador'
						    WHEN u.perfil_id = 2 THEN 'Residente'
						    WHEN u.perfil_id = 3 THEN 'Conductor'
						    WHEN u.perfil_id = 4 THEN 'Mecanico'
						    WHEN u.perfil_id = 5 THEN 'consultor'
						END as perfil
						FROM dl_usuario u
								inner join dl_tipo_identificacion ti
										on ti.tipo_identificacion_id = u.tipo_identificacion_id
								inner join dl_ciudad ci
										on ci.ciudad_id = u.ciudad_id
								inner join dl_eps e
										on e.eps_id = u.eps_id
								inner join dl_arl ar
										on ar.arl_id = u.arl_id
								inner join dl_afp af
										on af.afp_id = u.afp_id
								inner join dl_ccf cc
										on cc.ccf_id = u.ccf_id
								inner join dl_estado es
										on es.estado_id = u.estado_id
								inner join dl_empresa em
										on em.empresa_id = u.empresa_id
								where u.usuario_id = {$_SESSION['user']['usuario_id']} ";
	    $resultSetUsr = DB::query( $queryUsr );


		$queryArch = "SELECT * FROM dl_archivousuario where usuario_id = {$_SESSION['user']['usuario_id']} ";
		$resultSetArch = DB::query( $queryArch );


	    $datos['usr']	= $resultSetUsr[0];
	    $datos['arch']	= $resultSetArch;
	    return $datos;
	  }


	  public static function cambiar_pass( $p ){
	    include_once( '../../config/init_db.php' );

	    $key 	= Datos::$datos['key'];
	    $password   = "AES_ENCRYPT(MD5('".$p['password']."'),'".$key."')";

	    $queryUsr 	= "UPDATE dl_usuario set contrasena = $password  where usuario_id = {$_SESSION['user']['usuario_id']} ";
	    $resultSetUsr = DB::query( $queryUsr );

	    $respuesta = array();
		if( $resultSetUsr ){
			$respuesta['error'] = false;
			$respuesta['msj'] = 'Contrasena actualizada correctamente';
		}else{
			$respuesta['error'] = true;
			$respuesta['msj'] = 'No se pudo Actualizar la contrasena';
		}
	    return $respuesta;
	  }

}
