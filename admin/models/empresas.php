<?php
Class Empresas {

  /*Obtener los usuarios*/
  public static function select_Users( $p ){
	    include_once( '../../config/init_db.php' );

		$like           =   $p['search']['value']; // lo que se va a buscar en los campos
	    $limit          =   'limit '.$p['start'].','.$p['length'];  //limite a mostrar en la datatable
	    $Aux_order_by   =   $p['order'][0]['column']; // Campo por el qeu se va a ordenar la datatable
	    $asc_desc       =   $p['order'][0]['dir'];  // condicion por la qeu se va a ordenar el campo, desc asc

	    $order_by = ' order by ';

	    switch ( $Aux_order_by ) {
	    	case '0':
	    		$order_by .= 'empresa ';
	    		break;
	    	case '1':
	    		$order_by .= 'identification ';
	    		break;
	    	case '2':
	    		$order_by .= 'email ';
	    		break;
	    	case '3':
	    		$order_by .= 'phone ';
	    		break;
	    	case '4':
	    		$order_by .= 'address ';
	    		break;
	    	case '5':
	    		$order_by .= 'city ';
	    		break;
	    	case '6':
	    		$order_by .= 'profile ';
	    		break;
	    	case '7':
	    		$order_by .= 'headquarter ';
	    		break;
	    	case '8':
	    		$order_by .= 'first_name ';
	    		break;

	    }

	    $order_by .= $asc_desc;
	    $where     = " WHERE em.empresa like '%$like%' or em.tipo like '%$like%' ";

	    if( strpos($like, "+") ){
				$array = explode("+", $like);
					foreach ($array as $key => $value) {
						if( $key == 0){
								$where .= " or (em.empresa like '%$value%' or em.tipo like '%$value%') ";
						}else{
								$where .= " and (em.empresa like '%$value%' or em.tipo like '%$value%') ";
						}

					}
			}

	    $queryPer = "SELECT em.*, es.estado FROM dl_empresa em
							inner join dl_estado es
									on es.estado_id = em.estado_id
								$where
	    						$order_by ";
		$encontrados_total = DB::query( $queryPer );

		$encontrados = DB::query( $queryPer.$limit );

		foreach ($encontrados_total as $key => $value) {

			if( $value['estado_id'] == 1){
				$encontrados[$key]['estado'] = '<span class="text-success">'.$value['estado'].'</span>';
			}else if( $value['estado_id'] == 2){
				$encontrados[$key]['estado'] = '<span class="text-danger">'.$value['estado'].'</span>';
			}


				$encontrados[$key]['acciones'] = '<div data-id='.$value['empresa_id'].'>
										            <button type="button" class="editar btn btn-success btn-xs" data-toggle="modal" data-target="#myModal"><i class="fa fa-edit"></i></button>
										            <hr style="margin: 3px;">
										            <button type="button" class="addArchivo btn btn-warning btn-xs" data-toggle="modal" data-target="#Modal_archivos"><i class="fa fa-briefcase"></i></button>
										        </div>';

				$encontrados[$key]['logo'] = '<img src="../dist/img/empresas/'.$value['logo'].'" style="width: 100px;" alt="Grupo C2 SAS">';
				$encontrados[$key]['web'] = '<a href="'.$value['web'].'" target="_blank">'.$value['web'].'</a>';

		}


		$queryTotal = "SELECT count(*) as cantidad FROM dl_empresa";
		$counter 	= DB::query( $queryTotal );
		$counter	= $counter[0]['cantidad'];

		$datos = array();
		$datos['draw']              = $p['draw'];    // Consecutivo
	    $datos['recordsTotal']      = $counter;   // Total de regisrtos encontrados
	    $datos['recordsFiltered']   = count( $encontrados_total );    // Cantidad de resgistros encontrados
	    $datos['data']              = $encontrados; // datos encontrados

    return $datos;
  }

  /*Obtener los datos iniciales*/
	  public static function select_init(){
	    include_once( '../../config/init_db.php' );

		$queryEst = "SELECT * FROM dl_estado;";
		$resultSet_est = DB::query( $queryEst );

	    $datos['est']	= $resultSet_est;

	    return $datos;
	  }

	// Obtener datos de una empresa
	public static function consultar_empresa( $id ){

	    include_once( '../../config/init_db.php' );
	    $query = "SELECT * FROM dl_empresa where empresa_id = $id";
		$resultSet = DB::query( $query );
	    return $resultSet[0];
	  }

  /*Actualizar los usuarios*/
  public static function update_Empresa( $p, $prof = "../" ){
    include_once($prof.'../config/init_db.php');

    echo $queryPer = "UPDATE dl_empresa
							SET
							empresa 	= '{$p['empresa']}',
							tipo 		= '{$p['tipo']}',
							estado_id 	= '{$p['estado_id']}',
							web 		= '{$p['web']}',
							logo 		= '{$p['photo']}',
							membrete 	= '{$p['membrete']}'
							WHERE empresa_id = {$p['empresa_id']};";
		$resultSet_usr = DB::query( $queryPer );

		$respuesta = array();
		if( $resultSet_usr ){
			$respuesta['error'] = false;
			$respuesta['msj'] 	= 'Empresa actualizada correctamente';
		}else{
			$respuesta['error'] = true;
			$respuesta['msj'] 	= 'No se pudo actualizar la empresa';
		}
	    return json_encode($respuesta);
  }

  /*Crear empresas*/
  public static function insert_Empresa( $p ){
    include_once( '../../config/init_db.php' );

    /*$pass 	= $p['identificacion'];
    $key 	= 'Logistic_2018';*/

    // $password = "AES_ENCRYPT('$pass', '$key')";
    $queryPer = "INSERT INTO dl_empresa
						(
						empresa,
						logo,
						tipo,
						estado_id,
						web,
						membrete
						)
						VALUES
						(
						'{$p['empresa']}',
						'{$p['photo']}',
						'{$p['tipo']}',
						'{$p['estado_id']}',
						'{$p['web']}',
						'{$p['membrete']}'
					);";
		$resultSet_usr = DB::query( $queryPer );

		$respuesta = array();
		if( $resultSet_usr ){
			$respuesta['error'] = false;
			$respuesta['msj'] = 'Empresa creado correctamente';
		}else{
			$respuesta['error'] = true;
			$respuesta['msj'] = 'No se pudo crear ela empresa';
		}
	    return json_encode($respuesta);
  }

  /*Seleccionar un imagen de una empresa*/
  public static function select_img_ant( $p ){
    include_once( '../../config/init_db.php' );

	$queryPer = "SELECT logo FROM dl_empresa
					where empresa_id = {$p['empresa_id']};";
		$resultSet = DB::query( $queryPer );
	    return $resultSet[0]['logo'];
  }

  /*Inicio metodos archivos empresas*/

  /*Buscar archivos de las empresas*/
  public static function select_archivos( $p ){
    include_once( '../../config/init_db.php' );

	$queryPer = "SELECT archivoempresa_id, archivo, nombre FROM dl_archivoempresa
					where empresa_id = {$p['empresa_id']};";
		$resultSet = DB::query( $queryPer );

	    return json_encode($resultSet);
  }

  /*Seleccionar un archivo de una empresa*/
  public static function select_un_archivo( $p ){
    include_once( '../../config/init_db.php' );

	$queryPer = "SELECT archivo FROM dl_archivoempresa
					where archivoempresa_id = {$p['archivoempresa_id']};";
		$resultSet = DB::query( $queryPer );
	    return $resultSet[0]['archivo'];
  }


  /*Borrar archivos de una empresa*/
  public static function borrar_archivo_empresa( $archivoempresa_id ){
    include_once( '../../config/init_db.php' );

    	$query = "SELECT empresa_id FROM dl_archivoempresa
					where archivoempresa_id = $archivoempresa_id;";
				$resultSet_emp = DB::query( $query );

		$queryPer = "DELETE FROM dl_archivoempresa WHERE archivoempresa_id = $archivoempresa_id;";
		$resultSet = DB::query( $queryPer );

		$respuesta = array();
		if( $resultSet ){
			$respuesta['error'] = false;
			$respuesta['msj'] = 'Archivo borrado correctamente';
			$respuesta['empresa_id'] = $resultSet_emp[0]['empresa_id'];
		}else{
			$respuesta['error'] = true;
			$respuesta['msj'] = 'No se pudo borrar el archivo';
		}
	    return $respuesta;
  }


  /*Agregar archivo a una empresa*/
  public static function insert_Archivo( $p ){
    include_once( '../../config/init_db.php' );

	$query = "INSERT INTO dl_archivoempresa
							(
							empresa_id,
							archivo,
							nombre
							)
							VALUES
							(
							'{$p['empresa_id']}',
							'{$p['archivo']}',
							'{$p['nombre']}'
							);";
		$resultSet = DB::query( $query );

		$respuesta = array();
		if( $resultSet ){
			$respuesta['error'] = false;
			$respuesta['msj'] = 'Archivo agregado correctamente';
			$respuesta['empresa_id'] = $p['empresa_id'];
		}else{
			$respuesta['error'] = true;
			$respuesta['msj'] = 'No se pudo agregar archivo a la empresa';
		}

	    return json_encode($respuesta);
  }

  /*Fin metodos archivos empresas*/

}
