<?php
Class Frentes {

  /*Obtener los usuarios*/
  public static function select_Users( $p ){
	    include_once( '../../config/init_db.php' );
	    //DB::$encoding = 'utf8';

		$like           =   $p['search']['value']; // lo que se va a buscar en los campos
	    $limit          =   'limit '.$p['start'].','.$p['length'];  //limite a mostrar en la datatable
	    $Aux_order_by   =   $p['order'][0]['column']; // Campo por el qeu se va a ordenar la datatable
	    $asc_desc       =   $p['order'][0]['dir'];  // condicion por la qeu se va a ordenar el campo, desc asc

	    $order_by = ' order by ';

	    switch ( $Aux_order_by ) {
	    	case '0':
	    		$order_by .= 'nombre ';
	    		break;
	    	case '1':
	    		$order_by .= 'identification ';
	    		break;
	    	case '2':
	    		$order_by .= 'email ';
	    		break;
	    	case '3':
	    		$order_by .= 'phone ';
	    		break;
	    	case '4':
	    		$order_by .= 'address ';
	    		break;
	    	case '5':
	    		$order_by .= 'city ';
	    		break;
	    	case '6':
	    		$order_by .= 'profile ';
	    		break;
	    	case '7':
	    		$order_by .= 'headquarter ';
	    		break;
	    	case '8':
	    		$order_by .= 'first_name ';
	    		break;

	    }

	    $order_by .= $asc_desc;
	    $where     = " WHERE fr.nombre like '%$like%' or fr.contrato like '%$like%' ";

	    if( strpos($like, "+") ){
				$array = explode("+", $like);
					foreach ($array as $key => $value) {
						if( $key == 0){
								$where .= " or (fr.nombre like '%$value%' or fr.tipo contrato '%$value%') ";
						}else{
								$where .= " and (fr.nombre like '%$value%' or fr.tipo contrato '%$value%') ";
						}

					}
			}

	    $queryPer = "SELECT fr.*, ci.ciudad, dep.departamento, pa.pais, es.estado
	    					FROM .dl_frenteobra fr
								inner join dl_ciudad ci
											on fr.ciudad_id = ci.ciudad_id
								inner join dl_departamento dep
											on dep.departamento_id = ci.departamento_id
								inner join dl_pais pa
											on pa.pais_id = dep.pais_id
								inner join dl_estado es
											on es.estado_id = fr.estado_id
								$where
	    						$order_by ";
		$encontrados_total = DB::query( $queryPer );

		$encontrados = DB::query( $queryPer.$limit );

		foreach ($encontrados_total as $key => $value) {

				$encontrados[$key]['imagen'] = '<img src="../dist/img/frentes/'.$value['imagen'].'" style="width: 100px;" alt="Remodelación Parque la Ceiba">';

				$encontrados[$key]['nombre'] = $value['nombre'].'<br>
												Horario '.$value['hora_inicio'].' - '.$value['hora_fin'].'<br>
												Almuerzo '.$value['alm_inicio'].' - '.$value['alm_fin'];

				$encontrados[$key]['contrato'] = 'Contrato '.$value['contrato'];

				$encontrados[$key]['empresas'] = 'Pendiente';

				$encontrados[$key]['localizacion'] = ' '.$value['ciudad'].' | '.$value['departamento'].' | '.$value['pais'].'';

				if( $value['estado_id'] == 1){
						$encontrados[$key]['estado'] = '<span class="text-success">'.$value['estado'].'</span>';
					}else if( $value['estado_id'] == 2){
						$encontrados[$key]['estado'] = '<span class="text-danger">'.$value['estado'].'</span>';
					}

				$encontrados[$key]['acciones'] = '<div data-id='.$value['frenteobra_id'].'>
										            <button type="button" class="editar btn btn-success btn-xs" data-toggle="modal" data-target="#myModal"><i class="fa fa-edit"></i></button>
										            <button type="button" class="addArchivo btn btn-warning btn-xs" data-toggle="modal" data-target="#Modal_archivos"><i class="fa fa-briefcase"></i></button>
										            <hr style="margin: 3px;">
										            <button type="button" class="aliados btn btn-info btn-xs" data-toggle="modal" data-target="#Modal_aliados"><i class="fa fa-users"></i></button>
										        </div>';

		}


		$queryTotal = "SELECT count(*) as cantidad FROM dl_frenteobra";
		$counter 	= DB::query( $queryTotal );
		$counter	= $counter[0]['cantidad'];

		$datos = array();
		$datos['draw']              = $p['draw'];    // Consecutivo
	    $datos['recordsTotal']      = $counter;   // Total de regisrtos encontrados
	    $datos['recordsFiltered']   = count( $encontrados_total );    // Cantidad de resgistros encontrados
	    $datos['data']              = $encontrados; // datos encontrados

    return $datos;
  }

  /*Obtener los datos iniciales*/
	  public static function select_init(){
	    include_once( '../../config/init_db.php' );
	    DB::$encoding = 'utf8';

		$queryEst = "SELECT * FROM dl_estado;";
		$resultSet_est = DB::query( $queryEst );

		$queryCiu = "SELECT ciudad_id, ciudad FROM dl_ciudad where estado_id = 1 order by ciudad";
		$resultSet_ciu = DB::query( $queryCiu );

		$queryEmp = "SELECT * FROM dl_empresa where estado_id = 1;";
		$resultSet_Emp = DB::query( $queryEmp );

	    $datos['est']	= $resultSet_est;
	    $datos['ciu']	= $resultSet_ciu;
	    $datos['emp']	= $resultSet_Emp;

	    return $datos;
	  }

	// agregar un aliado
	public static function agregar_aliado( $p ){
	    include_once( '../../config/init_db.php' );
	    DB::$encoding = 'utf8';

		$queryFre = "INSERT INTO dl_empresafrente
							(
							frenteobra_id,
							empresa_id,
							tiposociedad
							)
							VALUES
							(
							'{$p['frenteobra_id']}',
							'{$p['empresa_id']}',
							'{$p['tiposociedad']}'
							);
							";
		$resultSet_fre = DB::query( $queryFre );

		$respuesta = array();
		if( $resultSet_fre ){
			$respuesta['error'] = false;
			$respuesta['msj'] 	= 'Aliado agregado correctamente';
			$respuesta['frenteobra_id'] = $p['frenteobra_id'];
		}else{
			$respuesta['error'] = true;
			$respuesta['msj'] 	= 'No se pudo agregar el aliado';
		}
	    return $respuesta;
	  }

	// Obtener datos de un frente de obra
	public static function consultar_frente( $frenteobra_id ){
	    include_once( '../../config/init_db.php' );
	    DB::$encoding = 'utf8';
	    $query = "SELECT * FROM dl_frenteobra where frenteobra_id = $frenteobra_id";
		$resultSet = DB::query( $query );

		$queryFre = "SELECT emf.*, em.empresa
						FROM dl_empresafrente emf
							inner join dl_empresa em
								on em.empresa_id = emf.empresa_id where frenteobra_id = $frenteobra_id ;";
		$resultSet_fre = DB::query( $queryFre );

		$array = array();
		$array['frente']  = $resultSet[0];
		$array['emp_fre'] = $resultSet_fre;
	    return $array;
	  }

  /*Actualizar los frentes de obra*/
  public static function update_Frente( $p, $prof = "../" ){
    include_once($prof.'../config/init_db.php');

    $queryPer = "UPDATE dl_frenteobra
							SET
							nombre 			= '{$p['nombre']}',
							hora_inicio 	= '{$p['hora_inicio']}',
							hora_fin 		= '{$p['hora_fin']}',
							alm_inicio 		= '{$p['alm_inicio']}',
							alm_fin 		= '{$p['alm_fin']}',
							contrato 		= '{$p['contrato']}',
							coordenadas 	= '{$p['coordenadas']}',
							ciudad_id 		= '{$p['ciudad_id']}',
							imagen 			= '{$p['imagen']}',
							estado_id 		= '{$p['estado_id']}',
							editor 			=  {$_SESSION['user']['usuario_id']},
							edicion 		=  now()
							WHERE frenteobra_id = '{$p['frenteobra_id']}';
							";
		$resultSet_usr = DB::query( $queryPer );

		$respuesta = array();
		if( $resultSet_usr ){
			$respuesta['error'] = false;
			$respuesta['msj'] 	= 'Empresa actualizada correctamente';
		}else{
			$respuesta['error'] = true;
			$respuesta['msj'] 	= 'No se pudo actualizar la empresa';
		}
	    return json_encode($respuesta);
  }

  /*Crear empresas*/
  public static function insert_Empresa( $p ){
    include_once( '../../config/init_db.php' );

    $queryPer = "INSERT INTO dl_frenteobra
							(
							nombre,
							hora_inicio,
							hora_fin,
							alm_inicio,
							alm_fin,
							contrato,
							coordenadas,
							ciudad_id,
							imagen,
							estado_id,
							editor,
							edicion
							)
							VALUES
							(
							'{$p['nombre']}',
							'{$p['hora_inicio']}',
							'{$p['hora_fin']}',
							'{$p['alm_inicio']}',
							'{$p['alm_fin']}',
							'{$p['contrato']}',
							'{$p['coordenadas']}',
							'{$p['ciudad_id']}',
							'{$p['imagen']}',
							'{$p['estado_id']}',
							 {$_SESSION['user']['usuario_id']},
							 now()
							);
							";
		$resultSet_usr = DB::query( $queryPer );

		$respuesta = array();
		if( $resultSet_usr ){
			$respuesta['error'] = false;
			$respuesta['msj'] = 'Frente de obra creado correctamente';
		}else{
			$respuesta['error'] = true;
			$respuesta['msj'] = 'No se pudo crear el frente de obra';
		}
	    return json_encode($respuesta);
  }


	/*Seleccionar un imagen de un frente*/
  public static function select_img_ant( $p ){
    include_once( '../../config/init_db.php' );

	$queryPer = "SELECT imagen FROM dl_frenteobra
					where frenteobra_id = {$p['frenteobra_id']};";
		$resultSet = DB::query( $queryPer );
	    return $resultSet[0]['imagen'];
  }


	/*Buscar empresas aliadas*/
  public static function buscar_aliados( $frenteobra_id ){
    include_once( '../../config/init_db.php' );

	$queryPer = "SELECT emf.*, em.empresa
						FROM dl_empresafrente emf
								inner join dl_empresa em
										on em.empresa_id = emf.empresa_id
										where frenteobra_id = $frenteobra_id;";
		$resultSet = DB::query( $queryPer );

		$array = array();
		foreach ($resultSet as $key => $value) {

			if( $value['tiposociedad'] == 1 ){
				$array[$key]['tiposociedad'] = 'Principal';
			}else if( $value['tiposociedad'] == 2 ){
				$array[$key]['tiposociedad'] = 'Secundaria';
			}

			$array[$key]['empresa'] = $value['empresa'];
			$array[$key]['empresafrente_id'] = $value['empresafrente_id'];

		}

	    return $array;
  }


  public static function borrar_aliado( $empresafrente_id ){
    include_once( '../../config/init_db.php' );

    	$query = "SELECT frenteobra_id FROM dl_empresafrente where empresafrente_id = $empresafrente_id;";
				$resultSet_emp = DB::query( $query );

		$queryPer = "DELETE FROM dl_empresafrente WHERE empresafrente_id = '$empresafrente_id';";
		$resultSet = DB::query( $queryPer );

		$respuesta = array();
		if( $resultSet ){
			$respuesta['error'] = false;
			$respuesta['msj'] = 'Aliado borrado correctamente';
			$respuesta['frenteobra_id'] = $resultSet_emp[0]['frenteobra_id'];
		}else{
			$respuesta['error'] = true;
			$respuesta['msj'] = 'No se pudo borrar el aliado';
		}
	    return $respuesta;
  }

  /*Inicio metodos archivos empresas*/

  /*Buscar archivos de las empresas*/
  public static function select_archivos( $p ){
    include_once( '../../config/init_db.php' );

	$queryPer = "SELECT * FROM dl_archivofrente
					where frenteobra_id = {$p['frenteobra_id']};";
		$resultSet = DB::query( $queryPer );

	    return json_encode($resultSet);
  }

  /*Seleccionar un archivo de una empresa*/
  public static function select_un_archivo( $p ){
    include_once( '../../config/init_db.php' );

	$queryPer = "SELECT archivo FROM dl_archivofrente where archivofrente_id = {$p['archivofrente_id']};";
		$resultSet = DB::query( $queryPer );
	    return $resultSet[0]['archivo'];
  }


  /*Borrar archivos de una empresa*/
  public static function borrar_archivo_empresa( $archivofrente_id ){
    include_once( '../../config/init_db.php' );

    	$query = "SELECT frenteobra_id FROM dl_archivofrente where archivofrente_id = $archivofrente_id;";
				$resultSet_emp = DB::query( $query );

		$queryPer = "DELETE FROM dl_archivofrente WHERE archivofrente_id = '$archivofrente_id';";
		$resultSet = DB::query( $queryPer );

		$respuesta = array();
		if( $resultSet ){
			$respuesta['error'] = false;
			$respuesta['msj'] = 'Archivo borrado correctamente';
			$respuesta['empresa_id'] = $resultSet_emp[0]['frenteobra_id'];
		}else{
			$respuesta['error'] = true;
			$respuesta['msj'] = 'No se pudo borrar el archivo';
		}
	    return $respuesta;
  }


  /*Agregar archivo a una empresa*/
  public static function insert_Archivo( $p ){
    include_once( '../../config/init_db.php' );

	$query = "INSERT INTO dl_archivofrente
						(
						frenteobra_id,
						archivo,
						nombre)
						VALUES
						(
						'{$p['frenteobra_id']}',
						'{$p['archivo']}',
						'{$p['nombre']}'
						);
						";
		$resultSet = DB::query( $query );

		$respuesta = array();
		if( $resultSet ){
			$respuesta['error'] = false;
			$respuesta['msj'] = 'Archivo agregado correctamente';
			$respuesta['frenteobra_id'] = $p['frenteobra_id'];
		}else{
			$respuesta['error'] = true;
			$respuesta['msj'] = 'No se pudo agregar archivo';
		}

	    return json_encode($respuesta);
  }

  /*Fin metodos archivos empresas*/

}
