<?php
Class Maquinaria {

  /*Obtener los usuarios*/
  public static function select_Users( $p ){
	    include_once( '../../config/init_db.php' );

		$like           =   $p['search']['value']; // lo que se va a buscar en los campos
	    $limit          =   'limit '.$p['start'].','.$p['length'];  //limite a mostrar en la datatable
	    $Aux_order_by   =   $p['order'][0]['column']; // Campo por el qeu se va a ordenar la datatable
	    $asc_desc       =   $p['order'][0]['dir'];  // condicion por la qeu se va a ordenar el campo, desc asc

	    $order_by = ' order by ';

	    switch ( $Aux_order_by ) {
	    	case '0':
	    		$order_by .= 'nombre ';
	    		break;
	    	case '1':
	    		$order_by .= 'identification ';
	    		break;
	    	case '2':
	    		$order_by .= 'email ';
	    		break;
	    	case '3':
	    		$order_by .= 'phone ';
	    		break;
	    	case '4':
	    		$order_by .= 'address ';
	    		break;
	    	case '5':
	    		$order_by .= 'city ';
	    		break;
	    	case '6':
	    		$order_by .= 'profile ';
	    		break;
	    	case '7':
	    		$order_by .= 'headquarter ';
	    		break;
	    	case '8':
	    		$order_by .= 'first_name ';
	    		break;

	    }

	    $order_by .= $asc_desc;
	    $where     = " WHERE tima.tipomaquina like '%$like%' or ma.placa like '%$like%' ";

	    if( strpos($like, "+") ){
				$array = explode("+", $like);
					foreach ($array as $key => $value) {
						if( $key == 0){
								$where .= " or ( tima.tipomaquina like '%$value%' or ma.placa like '%$value%') ";
						}else{
								$where .= " and ( tima.tipomaquina like '%$value%' or ma.placa like '%$value%') ";
						}

					}
			}

	    $queryPer = "SELECT ma.*, es.estado, tima.tipomaquina, fre.nombre as frenteobra FROM dl_maquinaria ma
								inner join dl_estado es
										on ma.estado_id = es.estado_id
					            inner join dl_tipomaquina tima
										on ma.tipomaquina_id = tima.tipomaquina_id
								inner join dl_frenteobra fre
										on fre.frenteobra_id = ma.frenteobra_id
								$where
	    						$order_by ";
		$encontrados_total = DB::query( $queryPer );

		$encontrados = DB::query( $queryPer.$limit );

		foreach ($encontrados_total as $key => $value) {

			if( $value['estado_id'] == 1){
				$encontrados[$key]['estado'] = '<span class="text-success">'.$value['estado'].'</span>';
			}else if( $value['estado_id'] == 2){
				$encontrados[$key]['estado'] = '<span class="text-danger">'.$value['estado'].'</span>';
			}


				$encontrados[$key]['acciones'] = '<div data-id='.$value['maquinaria_id'].'>
										            <button type="button" class="editar btn btn-success btn-xs" data-toggle="modal" data-target="#myModal"><i class="fa fa-edit"></i></button>
								                    <button type="button" class="btn btn-warning btn-xs"><i class="fa fa-calendar"></i></button>
								                    <button type="button" class="btn btn-info btn-xs"><i class="fa fa-wrench"></i></button>
								                    <button type="button" class="archivos btn btn-danger btn-xs" data-toggle="modal" data-target="#Modal_archivos"><i class="fa fa-image"></i></button>
										        </div>';

				$encontrados[$key]['fotografia'] = '<img src="../dist/img/maquinaria/'.$value['fotografia'].'" style="height: 80px;" alt="Maquinaria">';
				$encontrados[$key]['transito'] = '<span class="small">SOAT: '.$value['ult_soat'].'<br>TECN: '.$value['ult_tecno'].'</span>';
				$encontrados[$key]['observaciones'] = '<span class="text-danger small">Próximo cambio de aceite: 15.400 KM</span>';
				$encontrados[$key]['operador'] = '<i class="fa fa-user"></i> Diego A Lamprea M';

		}


		$queryTotal = "SELECT count(*) as cantidad FROM dl_maquinaria";
		$counter 	= DB::query( $queryTotal );
		$counter	= $counter[0]['cantidad'];

		$datos = array();
		$datos['draw']              = $p['draw'];    // Consecutivo
	    $datos['recordsTotal']      = $counter;   // Total de regisrtos encontrados
	    $datos['recordsFiltered']   = count( $encontrados_total );    // Cantidad de resgistros encontrados
	    $datos['data']              = $encontrados; // datos encontrados

    return $datos;
  }

  /*Obtener los datos iniciales*/
	  public static function select_init(){
	    include_once( '../../config/init_db.php' );

		$queryEst = "SELECT * FROM dl_estado;";
		$resultSet_est = DB::query( $queryEst );

		$query_fre = "SELECT frenteobra_id, nombre FROM dl_frenteobra;";
		$resultSetFre = DB::query( $query_fre );

		$query_maq = "SELECT tipomaquina_id, tipomaquina FROM dl_tipomaquina;";
		$resultSetMap = DB::query( $query_maq );




	    $datos['est']	= $resultSet_est;
	    $datos['fre']	= $resultSetFre;
	    $datos['maq']	= $resultSetMap;

	    return $datos;
	  }

	// Obtener datos de una empresa
	public static function consultar_maquinaria( $id ){
	    include_once( '../../config/init_db.php' );
	    DB::$encoding = 'utf8'; // defaults to latin1 if omitted
	    $query = "SELECT * FROM dl_maquinaria where maquinaria_id = $id";
		$resultSet = DB::query( $query );
	    return $resultSet[0];
	  }

  /*Actualizar los usuarios*/
  public static function update_maquinaria( $p, $prof = "../" ){
    include_once($prof.'../config/init_db.php');

    $queryPer = "UPDATE dl_maquinaria
							SET
							tipomaquina_id 		= '{$p['tipomaquina_id']},
							fotografia 			= '{$p['photo']},
							placa 				= '{$p['placa']},
							ult_soat 			= '{$p['ult_soat']},
							ult_tecno 			= '{$p['ult_tecno']},
							prox_man 			= '{$p['prox_man']},
							frenteobra_id 		= '{$p['frenteobra_id']},
							propiedad	 		= '{$p['propiedad']},
							localizacion 		= '{$p['localizacion']},
							observaciones 		= '{$p['observaciones']},
							estado_id 			= '{$p['estado_id']},
							editor 				= {$_SESSION['user']['usuario_id']},
							edicion 			= now()
							WHERE
							maquinaria_id = '{$p['maquinaria_id']};
							";
		$resultSet_usr = DB::query( $queryPer );

		$respuesta = array();
		if( $resultSet_usr ){
			$respuesta['error'] = false;
			$respuesta['msj'] 	= 'Maquinaria actualizada correctamente';
		}else{
			$respuesta['error'] = true;
			$respuesta['msj'] 	= 'No se pudo actualizar la maquinaria';
		}
	    return json_encode($respuesta);
  }

  /*Crear empresas*/
  public static function insert_Empresa( $p ){
    include_once( '../../config/init_db.php' );

    /*$pass 	= $p['identificacion'];
    $key 	= 'Logistic_2018';*/

    // $password = "AES_ENCRYPT('$pass', '$key')";
    $queryPer = "INSERT INTO dl_empresa
						(
						empresa,
						logo,
						tipo,
						estado_id,
						web,
						membrete
						)
						VALUES
						(
						'{$p['empresa']}',
						'{$p['photo']}',
						'{$p['tipo']}',
						'{$p['estado_id']}',
						'{$p['web']}',
						'{$p['membrete']}'
					);";
		$resultSet_usr = DB::query( $queryPer );

		$respuesta = array();
		if( $resultSet_usr ){
			$respuesta['error'] = false;
			$respuesta['msj'] = 'Empresa creado correctamente';
		}else{
			$respuesta['error'] = true;
			$respuesta['msj'] = 'No se pudo crear ela empresa';
		}
	    return json_encode($respuesta);
  }

  /*Seleccionar un imagen de una empresa*/
  public static function select_img_ant( $p ){
    include_once( '../../config/init_db.php' );

	$queryPer = "SELECT logo FROM dl_empresa
					where empresa_id = {$p['empresa_id']};";
		$resultSet = DB::query( $queryPer );
	    return $resultSet[0]['logo'];
  }

  /*Inicio metodos archivos empresas*/

  /*Buscar archivos de las empresas*/
  public static function select_archivos( $p ){
    include_once( '../../config/init_db.php' );

	$queryPer = "SELECT archivoempresa_id, archivo, nombre FROM dl_archivoempresa
					where empresa_id = {$p['empresa_id']};";
		$resultSet = DB::query( $queryPer );

	    return json_encode($resultSet);
  }

  /*Seleccionar un archivo de una empresa*/
  public static function select_un_archivo( $p ){
    include_once( '../../config/init_db.php' );

	$queryPer = "SELECT archivo FROM dl_archivoempresa
					where archivoempresa_id = {$p['archivoempresa_id']};";
		$resultSet = DB::query( $queryPer );
	    return $resultSet[0]['archivo'];
  }


  /*Borrar archivos de una empresa*/
  public static function borrar_archivo_empresa( $archivoempresa_id ){
    include_once( '../../config/init_db.php' );

    	$query = "SELECT empresa_id FROM dl_archivoempresa
					where archivoempresa_id = $archivoempresa_id;";
				$resultSet_emp = DB::query( $query );

		$queryPer = "DELETE FROM dl_archivoempresa WHERE archivoempresa_id = $archivoempresa_id;";
		$resultSet = DB::query( $queryPer );

		$respuesta = array();
		if( $resultSet ){
			$respuesta['error'] = false;
			$respuesta['msj'] = 'Archivo borrado correctamente';
			$respuesta['empresa_id'] = $resultSet_emp[0]['empresa_id'];
		}else{
			$respuesta['error'] = true;
			$respuesta['msj'] = 'No se pudo borrar el archivo';
		}
	    return $respuesta;
  }


  /*Agregar archivo a una empresa*/
  public static function insert_Archivo( $p ){
    include_once( '../../config/init_db.php' );

	$query = "INSERT INTO dl_archivoempresa
							(
							empresa_id,
							archivo,
							nombre
							)
							VALUES
							(
							'{$p['empresa_id']}',
							'{$p['archivo']}',
							'{$p['nombre']}'
							);";
		$resultSet = DB::query( $query );

		$respuesta = array();
		if( $resultSet ){
			$respuesta['error'] = false;
			$respuesta['msj'] = 'Archivo agregado correctamente';
			$respuesta['empresa_id'] = $p['empresa_id'];
		}else{
			$respuesta['error'] = true;
			$respuesta['msj'] = 'No se pudo agregar archivo a la empresa';
		}

	    return json_encode($respuesta);
  }

  /*Fin metodos archivos empresas*/

}
