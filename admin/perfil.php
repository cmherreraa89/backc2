<?php
$loc = "perfil";
include_once("src/mobiles.php");
require_once("src/seguridad.php");
?>
<!DOCTYPE html>
<html>
<head>
  <?php include_once("src/header.php"); ?>
</head>
<body class="hold-transition skin-green sidebar-mini">
<div class="wrapper">
  <?php include_once("src/main_header.php"); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Mi Perfil
        <small>A continuación encontrará la información del usuario actual o seleccionado</small>
      </h1>
      <!--<ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
      </ol>-->
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
    <!-- Main content -->


    <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
              <img class="profile-user-img img-responsive img-circle" src="../dist/img/usuarios/<?php echo($_SESSION['user']['fotografia']);?>" alt="<?php echo($_SESSION['user']['nombres']);?>">

              <h3 class="profile-username text-center"><?php echo($_SESSION['user']['nombres']);?> <?php echo($_SESSION['user']['apellidos']);?></h3>

              <p class="text-muted text-center"><?php echo($_SESSION['user']['cargo']);?></p>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Ingreso:</b> <a class="pull-right"><?php echo($_SESSION['user']['fecha_ingreso']);?></a>
                </li>
                <li class="list-group-item">
                  <b>Empresa:</b> <a class="pull-right">Grupo C2 SAS</a>
                </li>
                <li class="list-group-item">
                  <b>Horas de trabajo:</b> <a class="pull-right">1.254</a>
                </li>
              </ul>

              <!-- <a href="#" class="btn btn-success btn-block"><b>Editar</b></a> -->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <!-- About Me Box -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Información</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <strong><i class="fa fa-map-marker margin-r-5"></i> Contacto</strong>
              <p class="text-muted">
                Dirección: <?php echo($_SESSION['user']['direccion']);?><br>
                Teléfono: <?php echo($_SESSION['user']['telefono']);?><br>
                Celular: <?php echo($_SESSION['user']['celular']);?>
              </p>
              <hr>
              <strong><i class="fa fa-medkit margin-r-5"></i> Parafiscales</strong>
              <p class="text-muted">
                  ARL: Postiva<br>
                  EPS: EPS Sanitas<br>
                  AFP: Porvenir<br>
                  CCF: No Reporta
              </p>
              <hr>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#timeline" data-toggle="tab"><i class="fa fa-calendar"></i> Jornadas</a></li>
              <li><a href="#settings" data-toggle="tab"><i class="fa fa-briefcase"></i> Datos</a></li>
              <li><a href="#files" data-toggle="tab"><i class="fa fa-archive"></i> Archivos</a></li>
              <li><a href="#pass" data-toggle="tab"><i class="fa fa-key"></i> Contraseña</a></li>
            </ul>
            <div class="tab-content">

              <div class="active tab-pane" id="timeline">
                    <div class="box">
                        <div class="box-header">
                        <h3 class="box-title">Jornadas de trabajo Mes: Abril 2019</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding">
                        <table class="table table-condensed">
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>Día</th>
                                <th>Frente</th>
                                <th>Empresa</th>
                                <th>Programado</th>
                                <th>Ejecutado</th>
                                <th style="width: 40px">Tiempo</th>
                            </tr>
                            <tr>
                                <td>1.</td>
                                <td>01/04/2019</td>
                                <td>Apoyo Carretera a Vista Hermosa</td>
                                <td>Grupo C2</td>
                                <td><span class="text-green">07:30 - 17:30</span></td>
                                <td><span class="text-red">08:00 - 17:30</span></td>
                                <td>8.5H</td>
                            </tr>
                            <tr>
                                <td>2.</td>
                                <td>02/04/2019</td>
                                <td>Apoyo Carretera a Vista Hermosa</td>
                                <td>Grupo C2</td>
                                <td><span class="text-green">07:30 - 17:30</span></td>
                                <td><span class="text-green">07:30 - 17:30</span></td>
                                <td>9H</td>
                            </tr>
                            <tr>
                                <td>3.</td>
                                <td>03/04/2019</td>
                                <td>Apoyo Carretera a Vista Hermosa</td>
                                <td>Grupo C2</td>
                                <td><span class="text-green">07:30 - 17:30</span></td>
                                <td><span class="text-green">07:30 - 17:30</span></td>
                                <td>9H</td>
                            </tr>
                            <tr>
                                <td>4.</td>
                                <td>04/04/2019</td>
                                <td>Apoyo Carretera a Vista Hermosa</td>
                                <td>Grupo C2</td>
                                <td><span class="text-green">07:30 - 17:30</span></td>
                                <td><span class="text-yellow">07:00 - 17:30</span></td>
                                <td>9.5H</td>
                            </tr>
                            <tr>
                                <td>5.</td>
                                <td>05/04/2019</td>
                                <td>Apoyo Carretera a Vista Hermosa</td>
                                <td>Grupo C2</td>
                                <td><span class="text-green">07:30 - 17:30</span></td>
                                <td><span class="text-red">07:00 - 17:30</span></td>
                                <td>9H</td>
                            </tr>
                        </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
              </div>
              <!-- /.tab-pane -->

              <div class="tab-pane" id="settings">
                <form class="form-horizontal">
                  <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Nombre</label>

                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="nombres" placeholder="Nombre">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-2 control-label">Apellidos</label>

                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="apellidos" placeholder="Apellidos">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Identificacón</label>

                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="identificacion" placeholder="Name">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputExperience" class="col-sm-2 control-label">Email</label>

                    <div class="col-sm-10">
                      <textarea class="form-control" id="email" placeholder="Experience"></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputSkills" class="col-sm-2 control-label">Dirección</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="direccion" placeholder="Skills">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputSkills" class="col-sm-2 control-label">Telefono</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="telefono" placeholder="Skills">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputSkills" class="col-sm-2 control-label">Celular</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="celular" placeholder="Skills">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputSkills" class="col-sm-2 control-label">Ciudad</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="ciudad" placeholder="Skills">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputSkills" class="col-sm-2 control-label">eps</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="eps" placeholder="Skills">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputSkills" class="col-sm-2 control-label">Arl</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="arl" placeholder="Skills">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputSkills" class="col-sm-2 control-label">Afp</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="afp" placeholder="Skills">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputSkills" class="col-sm-2 control-label">Caja CF</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="ccf" placeholder="Skills">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputSkills" class="col-sm-2 control-label">Empresa</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="empresa" placeholder="Skills">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputSkills" class="col-sm-2 control-label">Cargo</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="cargo" placeholder="Skills">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputSkills" class="col-sm-2 control-label">Perfil</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="perfil" placeholder="Skills">
                    </div>
                  </div>
                  <!-- <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <div class="checkbox">
                        <label>
                          <input type="checkbox"> I agree to the <a href="#">terms and conditions</a>
                        </label>
                      </div>
                    </div>
                  </div> -->
                  <!-- <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <button type="submit" class="btn btn-danger">Submit</button>
                    </div>
                  </div> -->
                </form>
              </div>
              <!-- /.tab-pane -->

              <div class="tab-pane" id="files">
                <ul id="archivos" class="mailbox-attachments clearfix">
                    <li>
                    <span class="mailbox-attachment-icon"><i class="fa fa-file-pdf-o"></i></span>

                    <div class="mailbox-attachment-info">
                        <a href="#" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> AfiliaciónEPS.pdf</a>
                            <span class="mailbox-attachment-size">
                            </span>
                    </div>
                    </li>
                    <li>
                    <span class="mailbox-attachment-icon"><i class="fa fa-file-word-o"></i></span>

                    <div class="mailbox-attachment-info">
                        <a href="#" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> Contrato.docx</a>
                            <span class="mailbox-attachment-size">
                            </span>
                    </div>
                    </li>
                    <li>
                    <span class="mailbox-attachment-icon"><i class="fa fa-file-pdf-o"></i></span>

                    <div class="mailbox-attachment-info">
                        <a href="#" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> AfiliaciónARLAFC.pdf</a>
                            <span class="mailbox-attachment-size">
                            </span>
                    </div>
                    </li>
                    <li>
                    <span class="mailbox-attachment-icon"><i class="fa fa-file-pdf-o"></i></span>

                    <div class="mailbox-attachment-info">
                        <a href="#" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> LicenciaConduccion.pdf</a>
                            <span class="mailbox-attachment-size">
                            </span>
                    </div>
                    </li>
                </ul>
              </div>
              <!-- /.tab-pane -->

              <!-- tab-pane -->
              <div class="tab-pane" id="pass">
                <form class="form-horizontal" id="frm">
                  <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Contraseña</label>

                    <div class="col-sm-10">
                      <input type="password" class="form-control" name="password" id="password" placeholder="Contraseña">
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <button type="submit" class="btn btn-danger">Actualizar</button>
                    </div>
                  </div>
                </form>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>


    <!-- /.content -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include_once("src/main_footer.php"); ?>
</div>
<!-- ./wrapper -->
<?php include_once("src/footer.php"); ?>
<script src="js/perfil.js?v=<?php echo $sin_cache ?>"></script>
</body>
</html>