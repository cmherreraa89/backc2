$(document).ready(function() {
        Init();
} );

/*formulario crear editar*/
    $("#frm").submit(function(e){
        e.preventDefault();

          var data = new FormData();

          var other_data = $( this ).serializeArray();
          $.each(other_data,function(key,input){
              data.append(input.name,input.value);
          });

          data.append('opcn', 'cambiar_pass' );

          $.ajax({
            url:   'controllers/perfil.php',
            type: 'post',
            dataType: 'json',
            data: data,
            cache:        false,
            contentType:  false,
            processData:  false
          })
          .done(function( data ) {

              alert(data.msj)
          })
          .fail(function() {
            console.log("error");
          })

    });


function Init() {

            $.ajax({
                    data:  {opcn:'obtener_datos_iniciales'},
                    url:   'controllers/perfil.php',
                    type:  'POST',
                    dataType: "JSON",
                    success:  function (data) {

                        archivos = '';
                        for (var i = 0; i < data.arch.length; i++) {
                                archivos += `<li>
                                  <span class="mailbox-attachment-icon"><i class="fa fa-file-pdf-o"></i></span>

                                  <div class="mailbox-attachment-info">
                                      <a href="../dist/pdf/usuarios/`+data.arch[i].archivo+`" target="_blank" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> `+data.arch[i].nombre+`</a>
                                          <span class="mailbox-attachment-size">
                                          </span>
                                  </div>
                                  </li>`;
                            }

                        $('#archivos').html( archivos );

                        var usr = data.usr;

                        $('#identificacion').val( usr.identificacion );
                        $('#nombres').val( usr.nombres );
                        $('#apellidos').val( usr.apellidos );
                        $('#cargo').val( usr.cargo );
                        $('#fecha_ingreso').val( usr.fecha_ingreso );
                        $('#direccion').val( usr.direccion );
                        $('#telefono').val( usr.telefono );
                        $('#celular').val( usr.celular );
                        $('#email').val( usr.email );
                        $('#eps').val(usr.eps);
                        $('#tipo_identificacion').val(usr.tipo_identificacion);
                        $('#empresa').val(usr.empresa);
                        $('#arl').val(usr.arl);
                        $('#ccf').val(usr.ccf);
                        $('#ciudad').val(usr.ciudad);
                        $('#estado').val(usr.estado);
                        $('#afp').val(usr.afp);
                        $('#perfil').val(usr.perfil);
                    }//Fin succes

            });// fin $ajax
    }// fin function obtener_datos_iniciales

