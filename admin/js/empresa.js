$(document).ready(function() {
        Init();
} );

empresa_id   = '';


/*formulario crear editar*/
    $("#frm").submit(function(e){
        e.preventDefault();

          var data = new FormData();

          var file = document.getElementById('file-photo').files[0];
          var photo_new = file == undefined ? '' : file ;
              data.append('photo_new', photo_new );

          var other_data = $( this ).serializeArray();
          $.each(other_data,function(key,input){
              data.append(input.name,input.value);
          });

          data.append('opcn', opcn );
          data.append('empresa_id', empresa_id );
          data.append('photo', photo );

          ajax( data,  '../api/empresa');

    });

var table = $('#table-trabajadores').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "../api/empresa",
            "type": "POST"
        },
        "columns": [
            { "data": "logo" },
            { "data": "empresa" },
            { "data": "tipo" },
            { "data": "web" },
            { "data": "membrete" },
            { "data": "estado" },
            { "data": "acciones" }
        ]
    } );

function Init() {

            $.ajax({
                    data:  {opcn:'obtener_datos_iniciales'},
                    url:   '../api/empresa',
                    type:  'POST',
                    dataType: "JSON",
                    success:  function (data) {

                        sel_est = '<option value=""></option>';
                        for (var i = 0; i < data.est.length; i++) {
                              sel_est += '<option value="'+data.est[i].estado_id+'">'+data.est[i].estado+'</option>';
                            }
                        $('#estado_id').html( sel_est );

                    }//Fin succes

            });// fin $ajax
    }// fin function obtener_datos_iniciales



    function seleccion() {
        $(".select2").select2();
    }
    $('#crear').click(function() {
        setTimeout ("seleccion();", 500);
        $('#frm')[0].reset(); // borra el formulario
        $('#tipo-modal').html('Crear usuario');
        $('#enviar-datos').html('Crear');
        opcn = 'crear'; // variable global
        $("#photo").attr("src", "../dist/img/empresas/company-default.jpg");
    });

    $("#table-trabajadores").on("click", ".editar", function (e) {
        setTimeout ("seleccion();", 500);
        empresa_id  = $( this ).parents('div').attr('data-id');
        $('#tipo-modal').html('Datos actuales de la empresa');
        $('#enviar-datos').html('Actualizar');

        opcn = 'editar'; // variable global
        $.ajax({
            "url": "../api/empresa",
            type: 'post',
            dataType: 'json',
            data: { opcn: 'consultar_usuario', empresa_id: empresa_id},
        })
        .done(function( data ) {
                $('#logo').val( data.logo );
                $('#empresa').val( data.empresa );
                $('#web').val( data.web );
                $('#membrete').val( data.membrete );
                $('#estado_id').val(data.estado_id).trigger('change');
                $('#tipo').val(data.tipo).trigger('change');
                $("#photo").attr("src", "../dist/img/empresas/"+data.logo);
        })
        .fail(function() {
            console.log("error");
        })

    });


    //Inicio funcion para ajax
    function ajax(  datos, url){

        // show_animation();

        $.ajax({
              url:          url,
              data:         datos,
              cache:        false,
              contentType:  false,
              processData:  false,
              type:         'POST',
              dataType:     'JSON',
              success:  function (data) {
                  // show_animation();

                  $('#myModal').modal('hide');
                  Init();
                  if (!data.error) {
                    alert(data.msj);
                      // alert(data.msj);
                      table.ajax.reload();
                      $("#account-menu img").attr("src", data.photo);
                  }else{
                    alert(data.msj);
                      // alert(data.msj);
                  }
              }
          }).fail(function() {
            console.log('error')
          });// fin ajax

    } // Fin function ajax


    //Inico Cargar archivos

    /*formulario agregar archivos*/
    $("#frm_archivo").submit(function(e){
        e.preventDefault();
          var data = new FormData();

          var file = document.getElementById('archivo').files[0];
          var archivo = file == undefined ? '' : file ;
              data.append('archivo', archivo );

          var other_data = $( this ).serializeArray();
          $.each(other_data,function(key,input){
              data.append(input.name,input.value);
          });

          data.append('opcn', 'agregar_Archivo' );
          data.append('empresa_id', empresa_id );

          $.ajax({
              url:          '../api/empresa',
              data:         data,
              cache:        false,
              contentType:  false,
              processData:  false,
              type:         'POST',
              dataType:     'JSON',
              success:  function (data) {

                  if (!data.error) {
                      alert(data.msj);
                      $('#frm_archivo')[0].reset(); // borra el formulario
                      buscar_archivos( data.empresa_id );
                  }else{
                      alert(data.msj);
                  }
              }
          }).fail(function() {
            alert('error')
          });// fin ajax

    });

    $("#table-trabajadores").on("click", ".addArchivo", function (e) {
        $('#frm_archivo')[0].reset(); // borra el formulario
        empresa_id  = $( this ).parents('div').attr('data-id');
        buscar_archivos( empresa_id ); // busca las actividades economicas de esta empresa

    });

     function buscar_archivos( company_id  = '' ) {

            $("#table_archivos tbody").html( '' );

            $.ajax({
              data:  { opcn: "buscar_archivos", empresa_id: empresa_id},
              url:    '../api/empresa',
              type:  'POST',
              dataType: "JSON",
              success:  function (data) {

                    //Tabla representante legal
                    var filas = '';
                    for (var i = 0; i < data.length; i++) {

                          filas+= '<tr>';
                          filas+= '<td>'+data[i].nombre+'</td>';
                          filas+= '<td  data-archivoempresa_id="'+data[i].archivoempresa_id+'" >  <a class="btn btn-info btn-xs" href="../dist/pdf/empresas/'+data[i].archivo+'" target="_blank"><i data-toggle="tooltip" title="Ver archivo" class="fa fa-eye"></i></a> <button type="button" class="eliminar_archivo btn btn-danger btn-xs"><i data-toggle="tooltip" title="Borrar" class="fa fa-close"></i></button> </td>';
                          filas+= '</tr>';

                    }

                    filas = filas == '' ? 'Sin archivos osociados' : filas ;
                    $("#table_archivos tbody").html( filas );

              }// fin success

          }).fail(function() {
            alert('Algo salío mal, intenta nuevamente, si el inconveniente persiste comuicate con el area de soporte', 'warning');
          });// fin ajax

    }

    //Inicio eliminar archivo de empresa
    $("#table_archivos").on("click", ".eliminar_archivo", function (e) {
        if( !confirm('¿Estas seguro de eliminar?') ) { return; }
        var archivoempresa_id  =   $( this ).parents('td').attr('data-archivoempresa_id');
        borrar_archivo( archivoempresa_id  );
    });

    function borrar_archivo( archivoempresa_id ) {
        $.ajax({
              data:  { opcn: "borrar_archivo", archivoempresa_id: archivoempresa_id},
              url:    '../api/empresa',
              type:  'POST',
              dataType: "JSON",
              success:  function (data) {

                // Pintamos la tabla nuevamente
                if( data.error ){
                    alert(data.msj);
                    //alert( data.msj );
                }else{
                    alert(data.msj);
                    //alert( data.msj );
                    console.log( 'data'+data.company_id)
                    console.log( data )
                    buscar_archivos( data.company_id  );
                }

              }// fin success
            }).fail(function() {
            alert('Algo salío mal, intenta nuevamente, si el inconveniente persiste comuicate con el area de soporte', 'warning');
          });
    }
    //Fin borrar archivo
  //fin archivos


//previsualizar foto antes de subirla
// $(window).load(function(){

 $(function() {

  $('#file-photo').change(function(e) {
      addImage(e);
     });

     function addImage(e){
      var file = e.target.files[0],
      imageType = /image.*/;

      if (!file.type.match(imageType))
       return;

      var reader = new FileReader();
      reader.onload = fileOnload;
      reader.readAsDataURL(file);
     }

     function fileOnload(e) {
      var result=e.target.result;
      $('#photo').attr("src",result).hide().fadeIn('slow');;
     }

    });

    $('#editar-imagen').click(function(event) {
        $('#file-photo').click(); //simulamos un clic en el input type flie que camiara la imagen
    });

    $('#photo').click(function(event) {
        $('#file-photo').click(); //simulamos un clic en el input type flie que camiara la imagen
    });

  // });

