$(function() {
    /* initialize the external events
        -----------------------------------------------------------------*/
    function init_events(ele) {
        ele.each(function() {
            // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
            // it doesn't need to have a start or end
            var eventObject = {
                title: $.trim($(this).text()) // use the element's text as the event title
            }

            // store the Event Object in the DOM element so we can get to it later
            $(this).data('eventObject', eventObject)

            // make the event draggable using jQuery UI
            $(this).draggable({
                zIndex: 1070,
                revert: true, // will cause the event to go back to its
                revertDuration: 0 //  original position after the drag
            })

        })
    }

    init_events($('#external-events div.external-event'))

    /* initialize the calendar
        -----------------------------------------------------------------*/
    //Date for the calendar events (dummy data)
    var date = new Date()
    var d = date.getDate(),
        m = date.getMonth(),
        y = date.getFullYear()

    $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        buttonText: {
            today: 'hoy',
            month: 'mes',
            week: 'semana',
            day: 'día'
        },
        //Random default events
        events: [{
            title: 'Remodelación Parque la Ceiba',
            start: new Date(y, m, d - 4, 7, 30),
            end: new Date(y, m, d - 4, 17, 30),
            backgroundColor: '#00a65a',
            borderColor: '#00a65a',
            url: 'eventoDetalle?id=1'
        }, {
            title: 'Apoyo Carretera a Vista Hermosa',
            start: new Date(y, m, d - 4, 7, 30),
            end: new Date(y, m, d - 4, 18, 0),
            backgroundColor: '#dd4b39',
            borderColor: '#dd4b39',
            url: 'eventoDetalle?id=1'
        }, {
            title: 'Remodelación Parque la Ceiba',
            start: new Date(y, m, d - 3, 7, 30),
            end: new Date(y, m, d - 3, 17, 30),
            backgroundColor: '#00a65a',
            borderColor: '#00a65a',
            url: 'eventoDetalle?id=1'
        }, {
            title: 'Apoyo Carretera a Vista Hermosa',
            start: new Date(y, m, d - 3, 7, 30),
            end: new Date(y, m, d - 3, 18, 0),
            backgroundColor: '#00a65a',
            borderColor: '#00a65a',
            url: 'eventoDetalle?id=1'
        }, {
            title: 'Remodelación Parque la Ceiba',
            start: new Date(y, m, d - 2, 7, 30),
            end: new Date(y, m, d - 2, 17, 30),
            backgroundColor: '#00a65a',
            borderColor: '#00a65a',
            url: 'eventoDetalle?id=1'
        }, {
            title: 'Apoyo Carretera a Vista Hermosa',
            start: new Date(y, m, d - 2, 7, 30),
            end: new Date(y, m, d - 2, 18, 0),
            backgroundColor: '#00a65a',
            borderColor: '#00a65a',
            url: 'eventoDetalle?id=1'
        }, {
            title: 'Remodelación Parque la Ceiba',
            start: new Date(y, m, d - 1, 7, 30),
            end: new Date(y, m, d - 1, 17, 30),
            backgroundColor: '#00a65a',
            borderColor: '#00a65a',
            url: 'eventoDetalle?id=1'
        }, {
            title: 'Apoyo Carretera a Vista Hermosa',
            start: new Date(y, m, d - 1, 7, 30),
            end: new Date(y, m, d - 1, 18, 0),
            backgroundColor: '#f39c12',
            borderColor: '#f39c12',
            url: 'eventoDetalle?id=1'
        }, {
            title: 'Remodelación Parque la Ceiba',
            start: new Date(y, m, d, 7, 30),
            end: new Date(y, m, d, 17, 30),
            backgroundColor: '#00a65a',
            borderColor: '#00a65a',
            url: 'eventoDetalle?id=1'
        }, {
            title: 'Apoyo Carretera a Vista Hermosa',
            start: new Date(y, m, d, 7, 30),
            end: new Date(y, m, d, 18, 0),
            backgroundColor: '#dd4b39',
            borderColor: '#dd4b39',
            url: 'eventoDetalle?id=1'
        }],
        editable: false,
        droppable: false
    });
});