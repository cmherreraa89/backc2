$(document).ready(function() {
        Init();
} );

click_crear  = true; // esta variable determina si se dío clic en crear o en editar para mostrar mensaje de confirmación antes de editar el numero de identificacíón
usuario_id   = '';


/*formulario crear editar*/
    $("#frm").submit(function(e){
        e.preventDefault();

          var data = new FormData();

          var file = document.getElementById('file-photo').files[0];
          var photo_new = file == undefined ? '' : file ;
              data.append('photo_new', photo_new );

          var other_data = $( this ).serializeArray();
          $.each(other_data,function(key,input){
              data.append(input.name,input.value);
          });

          data.append('opcn', opcn );
          data.append('usuario_id', usuario_id );
          // data.append('photo', photo );

          ajax( data,  '../api/trabajador');

    });

var table = $('#table-trabajadores').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "../api/trabajador",
            "type": "POST"
        },
        "columns": [
            { "data": "fotografia" },
            { "data": "nombres" },
            { "data": "apellidos" },
            { "data": "identificacion" },
            { "data": "empresa" },
            { "data": "cargo" },
            { "data": "fecha_ingreso" },
            { "data": "bienestar" },
            { "data": "contacto" },
            { "data": "estado" },
            { "data": "acciones" }
        ]
    } );

function Init() {

            $.ajax({
                    data:  {opcn:'obtener_datos_iniciales'},
                    url:   '../api/trabajador',
                    type:  'POST',
                    dataType: "JSON",
                    success:  function (data) {

                        select_ciudades = '<option value=""></option>';
                        for (var i = 0; i < data.ciu.length; i++) {
                              select_ciudades += '<option value="'+data.ciu[i].ciudad_id+'">'+data.ciu[i].ciudad+'</option>';
                            }
                        $('#ciudad').html( select_ciudades );

                        /*Llenar select Tipo identificacion*/
                        select_tip_ide = '<option value=""></option>';
                        for (var i = 0; i < data.ti.length; i++) {
                              select_tip_ide += '<option value="'+data.ti[i].tipo_identificacion_id+'">'+data.ti[i].tipo_identificacion+'</option>';
                            }
                        $('#tipo_identificacion_id').html( select_tip_ide );

                        /* Select empresa*/
                        select_emp = '<option value=""></option>';
                        for (var i = 0; i < data.emp.length; i++) {
                              select_emp += '<option value="'+data.emp[i].empresa_id+'">'+data.emp[i].empresa+'</option>';
                            }
                        $('#empresa_id').html( select_emp );

                        select_arl = '<option value=""></option>';
                        for (var i = 0; i < data.arl.length; i++) {
                              select_arl += '<option value="'+data.arl[i].arl_id+'">'+data.arl[i].arl+'</option>';
                            }
                        $('#arl_id').html( select_arl );

                        select_eps = '<option value=""></option>';
                        for (var i = 0; i < data.eps.length; i++) {
                              select_eps += '<option value="'+data.eps[i].eps_id+'">'+data.eps[i].eps+'</option>';
                            }
                        $('#eps_id').html( select_eps );

                        select_caja = '<option value=""></option>';
                        for (var i = 0; i < data.caja.length; i++) {
                              select_caja += '<option value="'+data.caja[i].ccf_id+'">'+data.caja[i].ccf+'</option>';
                            }
                        $('#ccf_id').html( select_caja );

                        select_ciudad = '<option value=""></option>';
                        for (var i = 0; i < data.ciu.length; i++) {
                              select_ciudad += '<option value="'+data.ciu[i].ciudad_id+'">'+data.ciu[i].ciudad+'</option>';
                            }
                        $('#ciudad_id').html( select_ciudad );
                        $('#ciudad_nacimiento').html( select_ciudad ); 
                        $('#ciudad_exp').html( select_ciudad );

                        sel_est = '<option value=""></option>';
                        for (var i = 0; i < data.est.length; i++) {
                              sel_est += '<option value="'+data.est[i].estado_id+'">'+data.est[i].estado+'</option>';
                            }
                        $('#estado_id').html( sel_est );

                        sel_afp = '<option value=""></option>';
                        for (var i = 0; i < data.afp.length; i++) {
                              sel_afp += '<option value="'+data.afp[i].afp_id+'">'+data.afp[i].afp+'</option>';
                            }
                        $('#afp_id').html( sel_afp );

                        sel_banco = '<option value=""></option>';
                        for (var i = 0; i < data.banco.length; i++) {
                              sel_banco += '<option value="'+data.banco[i].banco_id+'">'+data.banco[i].banco+'</option>';
                            }
                        $('#banco').html( sel_banco );

                    }//Fin succes

            });// fin $ajax
    }// fin function obtener_datos_iniciales



    function seleccion() {
        $(".select2").select2();
    }
    $('#crear').click(function() {
        setTimeout ("seleccion();", 1000);
        $('#frm')[0].reset(); // borra el formulario
        $('#tipo-modal').html('Crear usuario');
        $('#enviar-datos').html('Crear');
        $('#identificacion').attr('readonly', false);
        $('#numero_cuenta').attr('readonly', false);
        click_crear = true;
        opcn = 'crear'; // variable global
        $("#photo").attr("src", "../dist/img/usuarios/default-profile.png");
    });

    $("#table-trabajadores").on("click", ".editar", function (e) {
        setTimeout ("seleccion();", 1000);
        usuario_id  = $( this ).parents('div').attr('data-id');
        $('#tipo-modal').html('Datos actuales del usuario');
        $('#enviar-datos').html('Actualizar');
        $('#identificacion').attr('readonly', true);
        $('#numero_cuenta').attr('readonly', true);

        click_crear = false; // variable global
        opcn = 'editar'; // variable global
        $.ajax({
            url: "../api/consultar_un_usuario",
            type: 'post',
            dataType: 'json',
            data: { usuario_id: usuario_id},
        })
        .done(function( data ) {
                $('#identificacion').val( data.identificacion );
                $('#nombres').val( data.nombres );
                $('#apellidos').val( data.apellidos );
                $('#cargo').val( data.cargo );
                $('#fecha_ingreso').val( data.fecha_ingreso );
                $('#direccion').val( data.direccion );
                $('#telefono').val( data.telefono );
                $('#celular').val( data.celular );
                $('#email').val( data.email );
                $('#eps_id').val(data.eps_id).trigger('change');
                $('#tipo_identificacion_id').val(data.tipo_identificacion_id).trigger('change');
                $('#empresa_id').val(data.empresa_id).trigger('change');
                $('#arl_id').val(data.arl_id).trigger('change');
                $('#ccf_id').val(data.ccf_id).trigger('change');
                $('#ciudad_id').val(data.ciudad_id).trigger('change');
                $('#estado_id').val(data.estado_id).trigger('change');
                $('#afp_id').val(data.afp_id).trigger('change');
                $('#perfil_id').val(data.perfil_id).trigger('change');
                $('#identificacion').val( data.identificacion );
                
                //campos agregados

                $('#ciudad_nacimiento').val(data.nacimiento_ciudad_id).trigger('change');
                $('#ciudad_exp').val(data.exp_ciudad_id).trigger('change'); 
                $('#fecha_expedicion').val( data.fecha_expedicion ); 
                $('#fecha_nacimiento').val( data.cumpleanos ); 
                $('#barrio').val( data.barrio ); 
                $('#nomina').val( data.nomina ); 
                $('#tarifa_arl').val( data.arl_tarifa ); 
                $('#sueldo_real').val( data.sueldo_real );
                $('#bonificacion').val( data.bonificacion ); 
                $('#tipo_bonificacion').val( data.tipo_bonificacion ).trigger('change'); 
                $('#tipo_cuenta').val( data.tipo_cuenta ); 
                $('#banco').val( data.banco_id ).trigger('change'); 
                $('#numero_cuenta').val( data.cuenta );  
                $('#fecha_retiro').val( data.fecha_retiro ); 
                $('#genero').val( data.genero ).trigger('change'); 

                $("#photo").attr("src", "../dist/img/usuarios/"+data.fotografia);

        })
        .fail(function() {
            console.log("error");
        })

    });

    $('#identificacion').click(function(event) {
      if( click_crear ){ return;}

      if(confirm('¿Estas seguro de modificar el numero de identificación?'))
        {
          click_crear = true; // variable global
          $( this ).attr('readonly', false);
        }
    });

    $('#numero_cuenta').click(function(event) {
      if( click_crear ){ return;}

      if(confirm('¿Estas seguro de modificar el numero de cuenta?'))
        {
          click_crear = true; // variable global
          $( this ).attr('readonly', false);
        }
    });



    //Inicio funcion para ajax
    function ajax(  datos, url){

        // show_animation();

        $.ajax({
              url:          url,
              data:         datos,
              cache:        false,
              contentType:  false,
              processData:  false,
              type:         'POST',
              dataType:     'JSON',
              success:  function (data) {
                  // show_animation();


                  if (!data.error) {

                    $('#myModal').modal('hide');
                    Init();
                    alert(data.msj);
                      // noty(data.msj);
                      table.ajax.reload();
                      $("#account-menu img").attr("src", data.photo);
                  }else{
                    alert(data.msj);
                      // noty(data.msj);
                  }
              }
          }).fail(function() {
            console.log('error')
            // noty('Algo salío mal, intenta nuevamente, si el inconveniente persiste comuicate con el area de soporte', 'warning');
            // show_animation();
          });// fin ajax

    } // Fin function ajax


    //Inico Cargar archivos

    /*formulario agregar archivos*/
    $("#frm_archivo").submit(function(e){
        e.preventDefault();
          var data = new FormData();

          var file = document.getElementById('archivo').files[0];
          var archivo = file == undefined ? '' : file ;
              data.append('archivo', archivo );

          var other_data = $( this ).serializeArray();
          $.each(other_data,function(key,input){
              data.append(input.name,input.value);
          });

          data.append('opcn', 'agregar_Archivo' );
          data.append('usuario_id', usuario_id );

          $.ajax({
              url:          '../api/trabajador',
              data:         data,
              cache:        false,
              contentType:  false,
              processData:  false,
              type:         'POST',
              dataType:     'JSON',
              success:  function (data) {

                  if (!data.error) {
                      alert(data.msj);
                      $('#frm_archivo')[0].reset(); // borra el formulario
                      buscar_archivos( data.usuario_id );
                  }else{
                      alert(data.msj);
                  }
              }
          }).fail(function() {
            alert('error')
          });// fin ajax

    });

    $("#table-trabajadores").on("click", ".addArchivo", function (e) {
        $('#frm_archivo')[0].reset(); // borra el formulario
        usuario_id  = $( this ).parents('div').attr('data-id');
        buscar_archivos( usuario_id ); // busca las actividades economicas de esta empresa

    });

     function buscar_archivos( usuario_id  = '' ) {

            $("#table_archivos tbody").html( '' );

            $.ajax({
              data:  { opcn: "buscar_archivos", usuario_id: usuario_id},
              url:    '../api/trabajador',
              type:  'POST',
              dataType: "JSON",
              success:  function (data) {

                    //Tabla representante legal
                    var filas = '';
                    for (var i = 0; i < data.length; i++) {

                          filas+= '<tr>';
                          filas+= '<td>'+data[i].nombre+'</td>';
                          filas+= '<td  data-archivousuario_id="'+data[i].archivousuario_id+'" >  <a class="btn btn-info btn-xs" href="../dist/pdf/usuarios/'+data[i].archivo+'" target="_blank"><i data-toggle="tooltip" title="Ver archivo" class="fa fa-eye"></i></a> <button type="button" class="eliminar_archivo btn btn-danger btn-xs"><i data-toggle="tooltip" title="Borrar" class="fa fa-close"></i></button> </td>';
                          filas+= '</tr>';

                    }

                    filas = filas == '' ? 'Sin archivos osociados' : filas ;
                    $("#table_archivos tbody").html( filas );

              }// fin success

          }).fail(function() {
            alert('Algo salío mal, intenta nuevamente, si el inconveniente persiste comuicate con el area de soporte', 'warning');
          });// fin ajax

    }

    //Inicio eliminar archivo de empresa
    $("#table_archivos").on("click", ".eliminar_archivo", function (e) {
        if( !confirm('¿Estas seguro de eliminar?') ) { return; }
        var archivousuario_id  =   $( this ).parents('td').attr('data-archivousuario_id');
        borrar_archivo( archivousuario_id  );
    });

    function borrar_archivo( archivousuario_id ) {

        $.ajax({
              data:  { opcn: "borrar_archivo", archivousuario_id: archivousuario_id},
              url:    '../api/trabajador',
              type:  'POST',
              dataType: "JSON",
              success:  function (data) {

                // Pintamos la tabla nuevamente
                if( data.error ){
                    alert(data.msj);
                    //alert( data.msj );
                }else{
                    alert(data.msj);
                    //alert( data.msj );
                    buscar_archivos( data.usuario_id  );
                }

              }// fin success
            }).fail(function() {
            alert('Algo salío mal, intenta nuevamente, si el inconveniente persiste comuicate con el area de soporte', 'warning');
          });
    }
    //Fin borrar archivo
  //fin archivos


//previsualizar foto antes de subirla
 $(function() {

  $('#file-photo').change(function(e) {
      addImage(e);
     });

     function addImage(e){
      var file = e.target.files[0],
      imageType = /image.*/;

      if (!file.type.match(imageType))
       return;

      var reader = new FileReader();
      reader.onload = fileOnload;
      reader.readAsDataURL(file);
     }

     function fileOnload(e) {
      var result=e.target.result;
      $('#photo').attr("src",result).hide().fadeIn('slow');;
     }

    });

    $('#editar-imagen').click(function(event) {
        $('#file-photo').click(); //simulamos un clic en el input type flie que camiara la imagen
    });

    $('#photo').click(function(event) {
        $('#file-photo').click(); //simulamos un clic en el input type flie que camiara la imagen
    });
