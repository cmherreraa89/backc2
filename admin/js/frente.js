$(document).ready(function() {
    Init();
});

frenteobra_id = '';

/*formulario crear editar*/
$("#frm_aliados").submit(function(e) {
    e.preventDefault();

    var data = new FormData();

    var other_data = $(this).serializeArray();
    $.each(other_data, function(key, input) {
        data.append(input.name, input.value);
    });

    data.append('opcn', 'agregar_aliado');
    data.append('frenteobra_id', frenteobra_id);

    $.ajax({
        url: '../api/frente',
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        type: 'POST',
        dataType: 'JSON',
        success: function(data) {

            if (!data.error) {
                alert(data.msj);
                $('#frm_aliados')[0].reset(); // borra el formulario
                buscar_aliados(data.frenteobra_id); // busca las actividades economicas de esta empresa
            } else {
                alert(data.msj);
            }
        }
    }).fail(function() {
        alert('error')
    }); // fin ajax

    // ajax( data,  '../api/frente');

});


/*formulario crear editar*/
$("#frm").submit(function(e) {
    e.preventDefault();

    var data = new FormData();

    var file = document.getElementById('file-photo').files[0];
    var photo_new = file == undefined ? '' : file;
    data.append('photo_new', photo_new);

    var other_data = $(this).serializeArray();
    $.each(other_data, function(key, input) {
        data.append(input.name, input.value);
    });

    data.append('opcn', opcn);
    data.append('frenteobra_id', frenteobra_id);
    // data.append('photo', photo );

    ajax(data, '../api/frente');

});

var table = $('#table-trabajadores').DataTable({
    "processing": true,
    "serverSide": true,
    "ajax": {
        "url": "../api/frente",
        "type": "POST"
    },
    "columns": [
        { "data": "imagen" },
        { "data": "nombre" },
        { "data": "contrato" },
        { "data": "empresas" },
        { "data": "localizacion" },
        { "data": "coordenadas" },
        { "data": "estado" },
        { "data": "acciones" }
    ]
});

function Init() {

    $.ajax({
        data: { opcn: 'obtener_datos_iniciales' },
        url: '../api/frente',
        type: 'POST',
        dataType: "JSON",
        success: function(data) {

                select_ciudades = '<option value=""></option>';
                for (var i = 0; i < data.ciu.length; i++) {
                    select_ciudades += '<option value="' + data.ciu[i].ciudad_id + '">' + data.ciu[i].ciudad + '</option>';
                }
                $('#ciudad_id').html(select_ciudades);

                sel_est = '<option value=""></option>';
                for (var i = 0; i < data.est.length; i++) {
                    sel_est += '<option value="' + data.est[i].estado_id + '">' + data.est[i].estado + '</option>';
                }
                $('#estado_id').html(sel_est);

                sel_emp = '<option value=""></option>';
                for (var i = 0; i < data.emp.length; i++) {
                    sel_emp += '<option value="' + data.emp[i].empresa_id + '">' + data.emp[i].empresa + '</option>';
                }
                $('#empresa_id').html(sel_emp);
                $(".select2").select2();
            } //Fin succes

    }); // fin $ajax
} // fin function obtener_datos_iniciales



function seleccion() {
    $(".select2").select2();
}
$('#crear').click(function() {
    setTimeout("seleccion();", 500);
    $('#frm')[0].reset(); // borra el formulario
    $('#tipo-modal').html('Crear usuario');
    $('#enviar-datos').html('Crear');
    opcn = 'crear'; // variable global
    $("#photo").attr("src", "../dist/img/frentes/frente-default.jpg");
});

$("body").on("click", ".aliados", function(e) {
    console.log('caa')
    $('#frm_aliados')[0].reset(); // borra el formulario
    frenteobra_id = $(this).parents('div').attr('data-id');
    frenteobra_id = $(this).parents('div').attr('data-id');
    buscar_aliados(frenteobra_id); // busca las actividades economicas de esta empresa

});

function buscar_aliados(frenteobra_id = '') {

    $("#table_aliados tbody").html('');
    $(".select2").select2();
    $.ajax({
        data: { opcn: "buscar_aliados", frenteobra_id_aliados: frenteobra_id },
        url: '../api/frente',
        type: 'POST',
        dataType: "JSON",
        success: function(data) {

                //Tabla representante legal
                var filas = '';
                for (var i = 0; i < data.length; i++) {

                    filas += '<tr>';
                    filas += '<td>' + data[i].empresa + '</td>';
                    filas += '<td>' + data[i].tiposociedad + '</td>';
                    filas += '<td  data-empresafrente_id="' + data[i].empresafrente_id + '" > <button type="button" class="eliminar_aliado btn btn-danger btn-xs"><i data-toggle="tooltip" title="Borrar" class="fa fa-close"></i></button> </td>';
                    filas += '</tr>';

                }

                filas = filas == '' ? 'Sin archivos osociados' : filas;
                $("#table_aliados tbody").html(filas);

            } // fin success

    }).fail(function() {
        alert('Algo salío mal, intenta nuevamente, si el inconveniente persiste comuicate con el area de soporte', 'warning');
    }); // fin ajax

}


$("body").on("click", ".eliminar_aliado", function(e) {
    if (!confirm('¿Estas seguro de eliminar?')) { return; }
    var empresafrente_id = $(this).parents('td').attr('data-empresafrente_id');
    console.log(empresafrente_id)
    borrar_aliado(empresafrente_id);
});

function borrar_aliado(empresafrente_id) {
    $.ajax({
        data: { opcn: "borrar_aliado", empresafrente_id: empresafrente_id },
        url: '../api/frente',
        type: 'POST',
        dataType: "JSON",
        success: function(data) {

                // Pintamos la tabla nuevamente
                if (data.error) {
                    alert(data.msj);
                    //alert( data.msj );
                } else {
                    alert(data.msj);
                    //alert( data.msj );
                    buscar_aliados(data.frenteobra_id);
                }

            } // fin success
    }).fail(function() {
        alert('Algo salío mal, intenta nuevamente, si el inconveniente persiste comuicate con el area de soporte', 'warning');
    });
}

$("#table-trabajadores").on("click", ".editar", function(e) {
    setTimeout("seleccion();", 500);
    frenteobra_id = $(this).parents('div').attr('data-id');
    $('#tipo-modal').html('Datos actuales del frente de obra');
    $('#enviar-datos').html('Actualizar');

    opcn = 'editar'; // variable global
    $.ajax({
            "url": "../api/frente",
            type: 'post',
            dataType: 'json',
            data: { opcn: 'consultar_frente', frenteobra_id: frenteobra_id },
        })
        .done(function(data) {

            var fre = data['frente']
            $('#alm_fin').val(fre.alm_fin);
            $('#alm_inicio').val(fre.alm_inicio);
            $('#contrato').val(fre.contrato);
            $('#coordenadas').val(fre.coordenadas);
            $('#frenteobra_id').val(fre.frenteobra_id);
            $('#hora_fin').val(fre.hora_fin);
            $('#hora_inicio').val(fre.hora_inicio);
            $('#nombre').val(fre.nombre);
            $('#estado_id').val(fre.estado_id).trigger('change');
            $('#ciudad_id').val(fre.estado_id).trigger('ciudad_id');
            $("#photo").attr("src", "../dist/img/frentes/" + fre.imagen);
        })
        .fail(function() {
            console.log("error");
        })

});


//Inicio funcion para ajax
function ajax(datos, url) {

    // show_animation();

    $.ajax({
        url: url,
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        type: 'POST',
        dataType: 'JSON',
        success: function(data) {
            // show_animation();

            $('#myModal').modal('hide');
            Init();
            if (!data.error) {
                alert(data.msj);
                // alert(data.msj);
                table.ajax.reload();
                $("#account-menu img").attr("src", data.photo);
            } else {
                alert(data.msj);
                // alert(data.msj);
            }
        }
    }).fail(function() {
        console.log('error')
    }); // fin ajax

} // Fin function ajax


//Inico Cargar archivos

/*formulario agregar archivos*/
$("#frm_archivo").submit(function(e) {
    e.preventDefault();
    var data = new FormData();

    var file = document.getElementById('archivo').files[0];
    var archivo = file == undefined ? '' : file;
    data.append('archivo', archivo);

    var other_data = $(this).serializeArray();
    $.each(other_data, function(key, input) {
        data.append(input.name, input.value);
    });

    data.append('opcn', 'agregar_Archivo');
    data.append('frenteobra_id', frenteobra_id);

    $.ajax({
        url: '../api/frente',
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        type: 'POST',
        dataType: 'JSON',
        success: function(data) {

            if (!data.error) {
                alert(data.msj);
                $('#frm_archivo')[0].reset(); // borra el formulario
                buscar_archivos(data.frenteobra_id);
            } else {
                alert(data.msj);
            }
        }
    }).fail(function() {
        alert('error')
    }); // fin ajax

});

$("#table-trabajadores").on("click", ".addArchivo", function(e) {
    $('#frm_archivo')[0].reset(); // borra el formulario
    frenteobra_id = $(this).parents('div').attr('data-id');
    buscar_archivos(frenteobra_id); // busca las actividades economicas de esta empresa

});

function buscar_archivos(frenteobra_id = '') {

    $("#table_archivos tbody").html('');

    $.ajax({
        data: { opcn: "buscar_archivos", frenteobra_id: frenteobra_id },
        url: '../api/frente',
        type: 'POST',
        dataType: "JSON",
        success: function(data) {

                //Tabla representante legal
                var filas = '';
                for (var i = 0; i < data.length; i++) {

                    filas += '<tr>';
                    filas += '<td>' + data[i].nombre + '</td>';
                    filas += '<td  data-archivofrente_id="' + data[i].archivofrente_id + '" >  <a class="btn btn-info btn-xs" href="../dist/pdf/empresas/' + data[i].archivo + '" target="_blank"><i data-toggle="tooltip" title="Ver archivo" class="fa fa-eye"></i></a> <button type="button" class="eliminar_archivo btn btn-danger btn-xs"><i data-toggle="tooltip" title="Borrar" class="fa fa-close"></i></button> </td>';
                    filas += '</tr>';

                }

                filas = filas == '' ? 'Sin archivos osociados' : filas;
                $("#table_archivos tbody").html(filas);

            } // fin success

    }).fail(function() {
        alert('Algo salío mal, intenta nuevamente, si el inconveniente persiste comuicate con el area de soporte', 'warning');
    }); // fin ajax

}

//Inicio eliminar archivo de empresa
$("#table_archivos").on("click", ".eliminar_archivo", function(e) {
    if (!confirm('¿Estas seguro de eliminar?')) { return; }
    var archivofrente_id = $(this).parents('td').attr('data-archivofrente_id');
    console.log(archivofrente_id)
    borrar_archivo(archivofrente_id);
});

function borrar_archivo(archivofrente_id) {
    $.ajax({
        data: { opcn: "borrar_archivo", archivofrente_id: archivofrente_id },
        url: '../api/frente',
        type: 'POST',
        dataType: "JSON",
        success: function(data) {

                // Pintamos la tabla nuevamente
                if (data.error) {
                    alert(data.msj);
                    //alert( data.msj );
                } else {
                    alert(data.msj);
                    //alert( data.msj );
                    buscar_archivos(data.frenteobra_id);
                }

            } // fin success
    }).fail(function() {
        alert('Algo salío mal, intenta nuevamente, si el inconveniente persiste comuicate con el area de soporte', 'warning');
    });
}
//Fin borrar archivo
//fin archivos


//previsualizar foto antes de subirla
// $(window).load(function(){

$(function() {

    $('#file-photo').change(function(e) {
        addImage(e);
    });

    function addImage(e) {
        var file = e.target.files[0],
            imageType = /image.*/;

        if (!file.type.match(imageType))
            return;

        var reader = new FileReader();
        reader.onload = fileOnload;
        reader.readAsDataURL(file);
    }

    function fileOnload(e) {
        var result = e.target.result;
        $('#photo').attr("src", result).hide().fadeIn('slow');;
    }

});

$('#editar-imagen').click(function(event) {
    $('#file-photo').click(); //simulamos un clic en el input type flie que camiara la imagen
});

$('#photo').click(function(event) {
    $('#file-photo').click(); //simulamos un clic en el input type flie que camiara la imagen
});

// });