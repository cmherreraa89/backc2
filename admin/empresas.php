<?php
$loc = "empr";
include_once("src/mobiles.php");
require_once("src/seguridad.php");
?>
<!DOCTYPE html>
<html>
<head>
  <?php include_once("src/header.php"); ?>
</head>
<style type="text/css" media="screen">
    @media (min-width: 992px) {
        .modal-lg {
            width: 1200px;
            height: 600px; /* control height here */
        }
    }


    .modal img {
          max-width: 25%;
          border-radius: 75px;
          /*border: 1px solid #aaa;*/
      }

      #imagen > p{
        color:#4ebfbb;
        cursor: pointer;
        font-weight: bold;
      }
  </style>
<body class="hold-transition skin-green sidebar-mini">
<div class="wrapper">
  <?php include_once("src/main_header.php"); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Empresas
        <small>En este módulo se administran las empresas con las cuales se tiene algún nivel de acuerdo, además de la empresa principal a quién pertenece la licencia de este sistema</small>
      </h1>
      <!--<ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
      </ol>-->
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
    <!-- Main content -->

    <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"><button id="crear" type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus-square"></i> Agregar Empresa</button></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="table-trabajadores" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Logo</th>
                  <th>Empresa</th>
                  <th>Tipo</th>
                  <th>Web</th>
                  <th>Membrete</th>
                  <th>Estado</th>
                  <th></th>
                </tr>
                </thead>

              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

            <!-- Modal -->
      <div class="modal fade" id="myModal" role="dialog">
                      <div class="modal-dialog modal-lg">

                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" id="tipo-modal">Datos actuales del usuario</h4>
                          </div>
                          <!-- Inicio formulario -->
                                <form autocomplete="off" id="frm">
                                      <div class="modal-body">

                                              <div class="row">
                                                <div class="col-md-3"></div>
                                                <div class="col-md-6">
                                                      <center>
                                                          <div id="imagen">
                                                            <img style="cursor: pointer;" id="photo" src="../dist/img/empresas/company-default.jpg" />
                                                            <input accept="image/*" name="file-photo" id="file-photo" type="file" style="display: none;" />
                                                            <p style="cursor: pointer;" id="editar-imagen">Editar <i class="fa fa-edit"></i></p>
                                                          </div>
                                                        </center>
                                                </div>
                                                <div class="col-md-3"></div>
                                              </div>


                                              <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                      <label for="empresa">Empresa</label>
                                                      <input type="text" class="form-control" id="empresa" name="empresa" placeholder="" autocomplete="off"  required>
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                   <!-- select -->
                                                      <div class="form-group">
                                                        <label for="tipo">Tipo</label>
                                                        <select id="tipo" name="tipo" data-placeholder="Seleccionar tipo" class="select2 form-control" tabindex="-1" required>
                                                          <option value=""></option>
                                                          <option value="Principal">Principal</option>
                                                          <option value="Aliada">Aliada</option>
                                                        </select>
                                                      </div>
                                                      <!-- select -->
                                                </div>

                                                <div class="col-md-3">
                                                  <div class="form-group">
                                                    <label for="web">Web</label>
                                                    <input type="text" class="form-control" id="web" name="web" placeholder="" autocomplete="off" required>
                                                  </div>
                                                </div>

                                                <div class="col-md-3">
                                                  <div class="form-group">
                                                    <label for="membrete">Membrete</label>
                                                    <input type="text" class="form-control" id="membrete" name="membrete" placeholder="" autocomplete="off" required>
                                                  </div>
                                                </div>

                                              </div>

                                              <!-- row -->
                                               <div class="row">

                                                <div class="col-md-3">
                                                    <!-- select -->
                                                      <div class="form-group">
                                                        <label for="estado_id">Estado</label>
                                                        <select id="estado_id" name="estado_id" data-placeholder="Selecionar Estado" class="select2 form-control" tabindex="-1" required>
                                                        </select>
                                                      </div>
                                                      <!-- select -->
                                                </div>

                                                <div class="col-md-9">
                                                </div>
                                              </div>
                                              <!-- ./row -->
                                      </div>
                                      <div class="modal-footer">
                                        <button id="enviar-datos" type="submit" class="btn btn-info">Actualizar</button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                      </div>

                                </form>
                                <!-- Fin formulario -->
                          <!-- End Modal content-->
                        </div>
                      </div>
                    </div>
        <!--/ Modal -->

        <!-- Modal_archivos -->
                      <div class="modal fade" id="Modal_archivos" role="dialog">
                        <div class="modal-dialog">

                          <!-- Modal content-->
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                              <h4 class="modal-title" id="tipo-modal">Archivos de la empresa</h4>
                            </div>
                            <div class="modal-body">

                              <!-- Inicio formulario -->
                                  <form autocomplete="off" id="frm_archivo">

                                    <div class="row">
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="nombre">Nombre archivo</label>
                                          <input type="text" class="form-control" id="nombre" name="nombre" autocomplete="off" required>
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                          <div class="form-group">
                                            <label for="archivo">Seleccionar archivo</label>
                                            <input type="file" class="form-control" id="archivo" name="archivo" accept="application/pdf" autocomplete="off" required>
                                          </div>
                                      </div>
                                    </div>

                                    <button type="submit" class="btn btn-info">Agregar</button>
                                  </form>
                                  <!-- Fin formulario -->

                                  <!-- Inicio table -->
                                    <h2>Archivos de esta empresa</h2>
                                    <!-- <p>The .table class adds basic styling (light padding and only horizontal dividers) to a table:</p> -->
                                    <div class="table-responsive">
                                    <table id="table_archivos" class="table">
                                      <thead>
                                        <tr>
                                          <th>Nombre</th>
                                          <th>Acciones</th>
                                        </tr>
                                      </thead>
                                      <tbody>

                                      </tbody>
                                    </table>
                                  </div>
                              <!-- fin tabla -->

                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            </div>

                            <!-- End Modal content-->
                          </div>
                        </div>

                       </div>
                       <!--Fin  Modal archivos -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include_once("src/main_footer.php"); ?>
</div>
<!-- ./wrapper -->
<?php include_once("src/footer.php"); ?>
<script src="js/empresa.js?v=<?php echo $sin_cache ?>"></script>
</body>
</html>