<?php

class Funciones_globales
{

	public function renombrar_y_subir_archivo($ruta, $tmp_name, $name, $iniciales='archivo', $archivo_anterior = '' )
	{


                    $archivo        =   $tmp_name; // capturamos el archivo temporal
                    $nombre_archivo =   $name; // capturamos el nombre del archivo temporal
                    $extencion      =   strtolower(substr( $nombre_archivo, -5 ) ); // Capruramos los 6 ultimos caracteres del nombre dle archivo y lo convertimos a minuscula
                    $array_ext      =   explode('.', $extencion); // partimos el nombre del archivo por el punto y armamos un array
                    $extencion      =   '.'.$array_ext[1]; // optenemos la extencion final o formato dle archivo
                    $hoy            =   getdate(); /*Funcion que captura fecha y hora en un array*/
                    $fecha          =   $hoy['year'].$hoy['mon'].$hoy['mday'].$hoy['hours'].$hoy['minutes'].$hoy['seconds'];
                    $nombre_final   =   $iniciales.'_'.$fecha.$extencion;

                    if ( copy ( $archivo, $ruta.$nombre_final ) ) {

                            $accion = $nombre_final;

                            if ( file_exists( $ruta.$archivo_anterior ) and $archivo_anterior != '' ) {

                                $respuesta['existe'] = "existe";
                                if( unlink( $ruta.$archivo_anterior ) ){
                                    $respuesta['borrado'] = "borrado";
                                }else{
                                    echo "no borrado";
                                }

                            }else{
                                //echo "no existe";
                            }

                        }else{

                            echo "no copiado";
                            $accion = false;
                        }
                    return $accion;
	}

    public function validar_session(){
        @session_start();

        $now = time();
        if( isset( $_SESSION['expire'] )) {  /*echo 'si'; echo "$now".'-'.$_SESSION['expire'];return;*/
            if( $now > $_SESSION['expire'] ){
                $_SESSION = array();
                if (ini_get("session.use_cookies")) {
                    $params = session_get_cookie_params();
                    setcookie(session_name(), '', time() - 42000,
                    $params["path"], $params["domain"],
                    $params["secure"], $params["httponly"]
                    );

                    if ( session_destroy() ) {
                        $json['mensaje'] = 'Por inactividad su sesión a terminado automaticamente, inicie nuevamente si desea continuar';
                        $json['url']     = 'index.html';
                        $json['error']   = true;
                        echo json_encode($json);
                        return false;
                    }else{
                        echo "no se pudo terminar la sesion";
                    }
                }

            }else{

                $_SESSION['expire'] = time() + (60*60); // Aumenta Session 60 minutos más
                return true;
            }

        }else{
                $_SESSION = array();
                if (ini_get("session.use_cookies")) {
                    $params = session_get_cookie_params();
                    setcookie(session_name(), '', time() - 42000,
                    $params["path"], $params["domain"],
                    $params["secure"], $params["httponly"]
                    );

                if ( session_destroy() ) {
                        $json['mensaje'] = 'Su sesión a caducado, inicie nuevamente si desea continuar';
                        $json['url'] = 'index.html';
                        $json['error'] = true;
                        echo json_encode($json);
                        return false;
                    }else{
                        echo "no se pudo terminar la sesion";
                        return false;
                    }
                }



            }

    }
}


 ?>