<?php

require_once("../src/seguridad.php");

$method = $_SERVER['REQUEST_METHOD'];
    switch ($method) {
        case 'POST':
            include_once('../models/frentes.php');
            //include_once('../models/usuarios2.php');
            /*print_r( $_POST);*/

            if ( isset( $_POST['start'] ) ) {

                    $json  = Frentes::select_Users( $_POST );

                    echo json_encode( $json );

                }else{

                    switch ($_POST['opcn']) {

                    case 'obtener_datos_iniciales':
                        $datos['pres_soci'] = Frentes::select_init();
                        $json['est']  = $datos['pres_soci']['est'];
                        $json['ciu']  = $datos['pres_soci']['ciu'];
                        $json['emp']  = $datos['pres_soci']['emp'];

                        echo json_encode( $json );
                    break;

                    case 'crear':

                         if( isset( $_FILES['photo_new'] ) ){
                            include_once('funciones_globales.php');
                            $obj_global = new Funciones_globales();

                            $ruta       =   '../../dist/img/frentes/';
                            $temporal   =   $_FILES['photo_new']['tmp_name'];
                            $nombre     =   $_FILES['photo_new']['name'];
                            $iniciales  =   'IMG_user';

                            $nombre_imagen = $obj_global->renombrar_y_subir_archivo($ruta , $temporal, $nombre, $iniciales, 'photo_new', '' );

                             if( !$nombre_imagen ){
                                    //$json['error']   = true;
                                    echo $json['mensaje'] = 'Error al subir imagen';
                                }else{
                                    //$json['mensaje'] = 'Subida';
                                    $_POST['imagen']  = $nombre_imagen;
                                    $usuarios = Frentes::insert_Empresa($_POST);
                                }

                        }else{
                            /*$json['mensaje'] = 'No enviaste foto';*/
                            $_POST['imagen'] = 'frente-default.jpg';
                            $usuarios = Frentes::insert_Empresa($_POST);
                        }

                        echo $usuarios;
                    break;
                    case 'consultar_frente':
                            $usuario = Frentes::consultar_frente($_POST['frenteobra_id']);
                            echo json_encode( $usuario );
                        break;

                    case 'editar':

                        if( isset( $_FILES['photo_new'] ) ){
                            include_once('funciones_globales.php');
                            $obj_global = new Funciones_globales();

                            $ruta       =   '../../dist/img/frentes/';
                            $temporal   =   $_FILES['photo_new']['tmp_name'];
                            $nombre     =   $_FILES['photo_new']['name'];
                            $iniciales  =   'IMG_frente';

                            $archivo_anterior = Frentes::select_img_ant( $_POST );
                            $borrar_archivo_ant = '';
                            if( $archivo_anterior != 'frente-default.jpg' ){
                                $borrar_archivo_ant = $archivo_anterior;
                            }

                            $nombre_imagen = $obj_global->renombrar_y_subir_archivo($ruta , $temporal, $nombre, $iniciales, $borrar_archivo_ant );

                             if( !$nombre_imagen ){

                                    //$json['error']   = true;
                                    echo $json['mensaje'] = 'Error al subir imagen';

                                }else{

                                    //$json['mensaje'] = 'Subida';
                                    $_POST['imagen']  = $nombre_imagen;

                                    $usuarios = Frentes::update_Frente($_POST);

                                }

                        }else{
                            /*$json['mensaje'] = 'No enviaste foto';*/
                            $usuarios = Frentes::update_Frente($_POST);
                        }

                        echo $usuarios;
                    break;

                    //Busca los archivos asosicados a una empresa
                    case 'buscar_archivos':
                        // preprint($_POST); return;
                        $archivos = Frentes::select_archivos( $_POST );

                        echo $archivos;
                    break;

                    //Borrar los archivos asosicados a una empresa
                    case 'borrar_archivo':

                        $ruta       =   '../../dist/pdf/frentes/';
                        $archivo_anterior = Frentes::select_un_archivo( $_POST );

                        if ( file_exists( $ruta.$archivo_anterior ) ) {

                            if( unlink( $ruta.$archivo_anterior ) ){

                                        $archivos = Frentes::borrar_archivo_empresa( $_POST['archivofrente_id'] );

                                        echo json_encode( $archivos ) ;
                                    }else{

                                        $json['error'] = true;
                                        $json['msj']   = 'No se pudo eliminar el archivo';

                                    }

                        }else{

                            $json['error'] = true;
                            $json['msj']   = 'Archivo no existe';
                            echo json_encode( $json );
                        }
                    break;

                    //Agregar archivos a una empresa
                        case 'agregar_Archivo':
                            // preprint($_POST);
                            // preprint($_FILES); return;
                            if( isset( $_FILES['archivo'] ) ){
                                include_once('funciones_globales.php');
                                $obj_global = new Funciones_globales();

                                $ruta       =   '../../dist/pdf/frentes/';
                                $temporal   =   $_FILES['archivo']['tmp_name'];
                                $nombre     =   $_FILES['archivo']['name'];
                                $iniciales  =   'FILES_company';

                                $nombre_Archivo = $obj_global->renombrar_y_subir_archivo($ruta , $temporal, $nombre, $iniciales, 'file', '' );

                                 if( !$nombre_Archivo ){

                                        echo 'Error al subir imagen';

                                    }else{

                                        $_POST['archivo']  = $nombre_Archivo;
                                        $usuarios = Frentes::insert_Archivo( $_POST );

                                        echo $usuarios;
                                    }

                            }else{

                                echo 'No enviaste archivo';
                            }

                        break;
                        case 'buscar_aliados':
                                $archivos = Frentes::buscar_aliados( $_POST['frenteobra_id_aliados'] );
                                echo json_encode( $archivos );
                            break;
                        case 'borrar_aliado':
                                $archivos = Frentes::borrar_aliado( $_POST['empresafrente_id'] );
                                echo json_encode( $archivos );
                            break;
                        case 'agregar_aliado':
                                $archivos = Frentes::agregar_aliado( $_POST );
                                echo json_encode( $archivos );
                            break;



            }// fin switch $_POST['accion']

            break; // fin case POST

            // case 'GET':
            //     /**/
            // break;// fin case GET

        } // Fin switch $method

                }
 ?>
