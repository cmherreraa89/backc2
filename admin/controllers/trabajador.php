<?php
require_once("../src/seguridad.php");
// print_r( $_POST);
// print_r( $_GET);
// return;
$method = $_SERVER['REQUEST_METHOD'];
    switch ($method) {
        case 'POST':
            include_once('../models/trabajadores.php');

            if ( isset( $_POST['start'] ) ) {

                    $json  = Trabajadores::select_Users( $_POST );

                    echo json_encode( $json );

                }else{

                    switch ($_POST['opcn']) {

                    case 'obtener_datos_iniciales':
                        $datos['pres_soci'] = Trabajadores::select_init();

                        $json['eps']  = $datos['pres_soci']['eps'];
                        $json['arl']  = $datos['pres_soci']['arl'];
                        $json['caja'] = $datos['pres_soci']['caja'];
                        $json['ti']   = $datos['pres_soci']['ti'];
                        $json['emp']  = $datos['pres_soci']['emp'];
                        $json['ciu']  = $datos['pres_soci']['ciu'];
                        $json['est']  = $datos['pres_soci']['est'];
                        $json['afp']  = $datos['pres_soci']['afp'];
                        $json['banco']  = $datos['pres_soci']['banco'];

                        echo json_encode( $json );
                    break;

                    case 'crear':

                         if( isset( $_FILES['photo_new'] ) ){
                            include_once('funciones_globales.php');
                            $obj_global = new Funciones_globales();

                            $ruta       =   '../files/photos_usr/'; // carpeta donde se va a copiar el archivo
                            $temporal   =   $_FILES['photo_new']['tmp_name']; // Archivo temporal del archivo
                            $nombre     =   $_FILES['photo_new']['name']; // nombre temppral del archivo
                            $iniciales  =   'IMG_user'; // Iniciales que se le quiere poner al archivo, esto es para renombrarlo

                            $nombre_imagen = $obj_global->renombrar_y_subir_archivo($ruta , $temporal, $nombre, $iniciales, false);

                             if( !$nombre_imagen ){
                                    echo $json['mensaje'] = 'Error al subir imagen';
                                }else{
                                    //$json['mensaje'] = 'Subida';
                                    $_POST['photo']  = $nombre_imagen;
                                    $usuarios = Trabajadores::insert_Usuarios($_POST);
                                }

                        }else{
                            //echo 'No enviaste foto';
                            $_POST['photo'] = 'default-profile.png';
                            $usuarios = Trabajadores::insert_Usuarios($_POST);
                        }

                        echo $usuarios;
                    break;
                    case 'consultar_usuario':
                            $usuario = Trabajadores::consultar_usuario($_POST['usuario_id']);
                            echo json_encode( $usuario );
                        break;

                    case 'editar':

                        $imagen_anterior = Trabajadores::select_img_ant( $_POST );

                        if( isset( $_FILES['photo_new'] ) ){
                            include_once('funciones_globales.php');
                            $obj_global = new Funciones_globales();

                            $ruta       =   '../../dist/img/usuarios/';
                            $temporal   =   $_FILES['photo_new']['tmp_name'];
                            $nombre     =   $_FILES['photo_new']['name'];
                            $iniciales  =   'IMG_user';

                            $borrar_archivo_ant = '';
                            if( $imagen_anterior != 'default-profile.png' ){
                                $borrar_archivo_ant = $imagen_anterior;
                            }

                            $nombre_imagen = $obj_global->renombrar_y_subir_archivo($ruta , $temporal, $nombre, $iniciales, $borrar_archivo_ant );

                             if( !$nombre_imagen ){

                                    //$json['error']   = true;
                                    echo $json['mensaje'] = 'Error al subir imagen';

                                }else{

                                    $_POST['fotografia']  = $nombre_imagen;
                                    $usuarios = Trabajadores::update_Usuarios($_POST);
                                }

                        }else{
                            $_POST['fotografia']  = $imagen_anterior;
                            $usuarios = Trabajadores::update_Usuarios($_POST);
                        }

                        echo $usuarios;
                    break;

                    //Busca los archivos asosicados a un trabajador
                    case 'buscar_archivos':
                        // preprint($_POST); return;
                        $archivos = Trabajadores::select_archivos( $_POST );

                        echo $archivos;
                    break;

                    //Borrar los archivos asosicados a un trabajador
                    case 'borrar_archivo':

                        $ruta       =   '../../dist/pdf/usuarios/';
                        $archivo_anterior = Trabajadores::select_un_archivo( $_POST );

                        if ( file_exists( $ruta.$archivo_anterior ) ) {

                            if( unlink( $ruta.$archivo_anterior ) ){

                                        $archivos = Trabajadores::borrar_archivo_usuario( $_POST['archivousuario_id'] );

                                        echo json_encode( $archivos ) ;
                                    }else{

                                        $json['error'] = true;
                                        $json['msj']   = 'No se pudo eliminar el archivo';

                                    }

                        }else{

                            $json['error'] = true;
                            $json['msj']   = 'Archivo no existe';
                            echo json_encode( $json );
                        }
                    break;

                    //Agregar archivos a un usuario
                        case 'agregar_Archivo':
                            if( isset( $_FILES['archivo'] ) ){
                                include_once('funciones_globales.php');
                                $obj_global = new Funciones_globales();

                                $ruta       =   '../../dist/pdf/usuarios/';
                                $temporal   =   $_FILES['archivo']['tmp_name'];
                                $nombre     =   $_FILES['archivo']['name'];
                                $iniciales  =   'FILES_usuario';

                                $nombre_Archivo = $obj_global->renombrar_y_subir_archivo($ruta , $temporal, $nombre, $iniciales, false);

                                 if( !$nombre_Archivo ){

                                        echo 'Error al subir imagen';

                                    }else{

                                        $_POST['archivo']  = $nombre_Archivo;
                                        $usuarios = Trabajadores::insert_Archivo( $_POST );

                                        echo $usuarios;
                                    }

                            }else{

                                echo 'No enviaste archivo';
                            }

                        break;

            }// fin switch $_POST['accion']

            break; // fin case POST

            // case 'GET':
            //     /**/
            // break;// fin case GET

        } // Fin switch $method

                }
 ?>
